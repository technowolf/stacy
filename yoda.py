import requests
def yodish(text):
    url = f"http://yoda-api.appspot.com/api/v1/yodish?text={text}"
    response = requests.request("GET", url).json()
    return response.get('yodish')