import requests
from passwd_dict import dict_call
def udict(word):
    url = "https://mashape-community-urban-dictionary.p.rapidapi.com/define"

    querystring = {"term":f"{word}"}

    headers = {
        'x-rapidapi-key': f"{dict_call('Urban')}",
        'x-rapidapi-host': "mashape-community-urban-dictionary.p.rapidapi.com"
        }

    response = requests.request("GET", url, headers=headers, params=querystring).json()
    z = 0
    try:
        for x in response.get('list')[0].get('definition'):

            z += 1

        define = response.get('list')[0].get('definition')
        if z > 1000:

            return define[0:1000]
        else:

            return define
    except:
        return "This word has no meaning whatsoever... Please check your word and try again."

