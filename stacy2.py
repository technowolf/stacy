import math
import requests
import discord
from discord import Embed , Webhook, RequestsWebhookAdapter
from discord.ext import commands
from youtube_dl import YoutubeDL
from youtube_search import YoutubeSearch
import sqlite3
import aiohttp
import passwd_dict
import os
import asyncio
import base64
import random
from googletrans import Translator
from discord_slash import SlashCommand, cog_ext
from discord_slash.utils.manage_commands import create_choice, create_option
from discord_slash.utils.manage_components import create_button, create_actionrow, create_select, create_select_option, wait_for_component
from discord_slash.model import ButtonStyle
from discord_slash.context import ComponentContext
import string
from akinator.async_aki import Akinator
import akinator
import datetime
import threading

t = Translator()

token = passwd_dict.dict_call('token')
intents = discord.Intents.all()
intents.members = True
intents.guilds = True
bot = commands.Bot(command_prefix=commands.when_mentioned_or("Stacy ","stacy ","stacy","Stacy"),intents=intents,case_insensitive=True, guild_subscriptions=True)
slash = SlashCommand(bot, sync_commands=True)
conn = sqlite3.connect('music.db')
bot_db = sqlite3.connect('bot.db')
spotify_creds = passwd_dict.dict_call('Spotify')
auth = f"{spotify_creds.get('client_id')}:{spotify_creds.get('client_secret')}"
auth = auth.encode()
auth = base64.b64encode(auth)
auth = auth.decode()


class guild_queue():
    queue = {}
    dqueue = {}
    def __init__(self,server,user,data,number=1):
        if guild_queue.queue.get(server) == None:
            guild_queue.queue[server] = {'loop':False,'playing':0,'first_command':None,'stopped':None}
            guild_queue.dqueue[server] = {}
        else:
            number = len(guild_queue.queue.get(server)) - 4
        self.server = server
        self.user = user
        self.data = data
        self.number = number
        guild_queue.queue_add(self,server)
        guild_queue.display_queue(self,server)

    def queue_add(self,server):
        music_queue = guild_queue.queue.get(server)
        music_queue[len(music_queue)-3] = self
    
    def display_queue(self,server):
        guild_display_queue = guild_queue.dqueue.get(server)
        guild_display_queue[len(guild_display_queue)+1] = self.data[0].get('title')

class users():
    added_users = {}

async def register_channel(ctx, message_id):
    c = conn.cursor()
    c.execute("""SELECT * FROM channel WHERE guild_id = ?""",[ctx.guild.id])
    data = c.fetchone()
    if data == None:
        c.execute("""INSERT INTO channel (guild_id, channel_id, message_id) VALUES (?,?,?) """,[ctx.guild.id, ctx.message.channel.id, message_id])
        conn.commit()
    else:
        try:
            channel = await bot.fetch_channel(ctx.message.channel.id)
            message = await channel.fetch_message(data[-1])
            await message.delete()
        except:
            pass
        c.execute("""UPDATE channel SET channel_id = ? , message_id = ? WHERE guild_id = ? """,[ctx.message.channel.id, message_id, ctx.guild.id])
        conn.commit()

async def channel_check(channel_id):
    c = conn.cursor()
    c.execute("""SELECT * FROM channel WHERE channel_id = ?""",[channel_id])
    data = c.fetchone()
    if data == None:
        return False
    else:
        return True

async def url_sorter(url,message_guild=None,author=None,added=False):
    if url.startswith('https://www.youtube.com/'):
        main_url = await YouTube_Searcher(name=url,number=1)
        return main_url
    elif url.startswith('https://open.spotify.com/playlist/'):
        playlist_id = url.split('/')[-1].split('?')[0]
        names = await playlist_songs(playlist_id=playlist_id)
        for name in names:
            main_url = await YouTube_Searcher(name=name,number=1)
            if added == False:
                return main_url
            if added == True and name == names[0]:
                continue    
            guild_queue(message_guild,author,main_url)
        return None
    elif url.startswith('https://open.spotify.com/'):
        name = await SpotifySearcher(url)
        main_url = await YouTube_Searcher(name=name,number=1)
        return main_url
    
async def get_message_id(guild_id):
    c = conn.cursor()
    c.execute("""SELECT message_id FROM channel WHERE guild_id = ?""",[guild_id])
    message_id = c.fetchone()
    if message_id != None:
        return message_id[0]
    else:
        return None

async def get_channel_id(guild_id):
    c = conn.cursor()
    c.execute("""SELECT channel_id FROM channel WHERE guild_id = ?""",[guild_id])
    channel_id = c.fetchone()
    if channel_id != None:
        return channel_id[0]
    else:
        return None

async def queue_list(server_id,current_number=0):
    server_queue = guild_queue.dqueue.get(server_id)
    queue_to_display = ""
    add = 0
    for songs in server_queue:
        if songs > current_number:
            queue_to_display += f"{songs}: {server_queue.get(songs)}\n"
            add += 1
            if add > 5:
                break
    if len(queue_to_display) > 0:
        return f'''```python\n{queue_to_display}```'''
    else:
        return "No songs in queue <a:heh:876225355088797736>"

async def server_message(guild_id):
    message_id = await get_message_id(guild_id)
    channel_id = await get_channel_id(guild_id)
    channel = await bot.fetch_channel(channel_id)
    message_object = await channel.fetch_message(message_id)
    return message_object

async def song_embed(guild_id,status="Playing",current_number=0):
    main_queue = guild_queue.queue.get(guild_id)
    playing = main_queue.get('playing')
    loop = main_queue.get('loop')
    if loop == 'one':
        loop = "One"
    elif loop == False:
        loop = "Off"
    elif loop:
        loop = "Queue"
    
    queue = await queue_list(guild_id,playing)
    data = main_queue.get(playing).data[0]
    img = data.get('thumbnails')[-1]
    title = data.get('title')
    duration = data.get('duration')
    views = data.get('views')
    if status.capitalize() == 'Playing':
        button_status = "Pause"
    else:
        button_status = "Play"
    embed = Embed(title=f"Now {status}",color=0x3bff6f)
    embed.add_field(name="Name",value=title,inline=False)
    embed.add_field(name="Duration",value=duration,inline=True)
    embed.add_field(name="Views",value=views,inline=True)
    embed.add_field(name="URL",value=f"https://www.youtube.com{data.get('url_suffix')}",inline=False)
    try:        
        embed.add_field(name="Queue",value=queue,inline=False)
    except:
        pass
    embed.add_field(name="Channel",value=f"{data.get('channel')}",inline=True)
    embed.add_field(name="Loop",value=f"{loop}",inline=True)
    embed.set_footer(text=f"Song requested by {main_queue.get(playing).user}")
    embed.set_image(url=img)
    message = await server_message(guild_id)
    buttons = []
    emoji_list = ['⏮️','Clear',f'{button_status}','Loop','⏭️']
    for emoji in emoji_list:
        buttons.append(create_button(style=ButtonStyle.blue, label=f'{emoji}'))
    action_row = create_actionrow(*buttons)
    await message.edit(embed=embed,components=[action_row])

async def YouTube_Searcher(name,number=1):
    results = YoutubeSearch(f'{name}', max_results=number).to_dict()
    return results

async def SpotifySearcher(url):
    try:
        async with aiohttp.ClientSession() as session:
            track_id = url[url.find('/track/')+len('/track/'):]
            if track_id.find('?') != -1:
                track_id = track_id[:track_id.find('?')]
            headers = {"Accept":"application/json" ,"Content-Type":"application/json","Authorization": f"Bearer {music.access_token}"}
            async with session.get(url=f"https://api.spotify.com/v1/tracks/{track_id}",headers=headers) as response:
                response = await response.json()
                name = response.get('name')
                if name != None:
                    try:
                        artists = response.get('artists')
                        if artists != None:
                            suffix = " By "
                            for artist in artists:
                                suffix += f"{artist.get('name')}"
                            return name + suffix
                        return name 
                    except Exception as e:
                        print(e)
                        return "Rick Astley Never Gonna Give You Up"
                else:
                    return "Rick Astley Never Gonna Give You Up"
    except:
        pass

async def url_extract(data):
    return f"https://www.youtube.com{data.get('url_suffix')}"

async def validator(message):
    try:
        check = await channel_check(message.channel.id)
        check2 = (message.author.bot == False)
        try:
            check3 = guild_queue.queue.get(message.guild.id).get('stopped') == False
        except:
            check3 = True
        if check and check2 and (check3 != None and check3):
            return True
        else:
            return False
    except:
        return False

async def get_spotify_token(method=0):
    try:
        async with aiohttp.ClientSession() as session:
            if method == 0:
                data = {'grant_type':'authorization_code','code':code,'redirect_uri':'https://stevediscord.cf'}
            else:
                data = {'grant_type':'refresh_token','refresh_token':music.refresh_token}

            headers = {'Authorization': f'Basic {auth}'}
            url = "https://accounts.spotify.com/api/token"
            async with session.post(url=url,data=data,headers=headers) as response:
                response = await response.json()
                if response.get('access_token') != None:
                    if method == 0:
                        music.refresh_token = response.get('refresh_token')
                        with open('./token.txt','w') as file:
                            file.write({'ref':response.get('refresh_token'),'access':response.get('access_token')})
                            file.close()
                    music.access_token = response.get('access_token')
                    print("Got Auth Token")
                else:
                    print("Failed")
    except:
        pass

async def playlist_songs(playlist_id="3poMGDmYnyVQdQGYLtbj8W"):
    try:
        async with aiohttp.ClientSession() as session:
            headers = {"Accept":"application/json" ,"Content-Type":"application/json","Authorization": f"Bearer {music.access_token}"}
            url = f"https://api.spotify.com/v1/playlists/{playlist_id}"
            async with session.get(url=url,headers=headers) as response:
                response = await response.json()
                items = response.get('tracks').get('items')
                all_songs = []
                for item in items:
                    track_items = item.get('track')
                    name = track_items.get('name')
                    all_songs.append(name)
                return all_songs                    
    except Exception as e:
        print(e)

async def add_role(ctx,user,roleid):
    pass

async def remove(ctx,user,roleid):
    pass

async def register_translator(ctx,language):
    c = bot_db.cursor()
    language_code = await language_validator(language)
    c.execute("""SELECT * FROM translator WHERE guild = ? AND channelid = ?""",[ctx.guild.id,ctx.message.channel.id])
    channel = c.fetchone()
    if channel == None:
        c.execute("""INSERT INTO translator (guild, channelid, language) VALUES (?,?,?)""",[ctx.guild.id,ctx.message.channel.id,language_code])
        bot_db.commit()
        await ctx.send(embed=Embed(description=f'Registered {ctx.message.channel} for {language}',color=0x03fc5a))
    else:
        c.execute("""UPDATE translator SET language = ? WHERE channelid = ? """,[language_code, ctx.message.channel.id])
        bot_db.commit()
        await ctx.send(embed=Embed(description=f'Updated {ctx.message.channel} for {language}',color=0x03fc5a))

async def language_validator(language):
    language = str(language).capitalize()
    langs = {'Afrikaans':'af','Albanian':'sq','Amharic':'am','Arabic':'ar','Armenian':'hy','Azerbaijani':'az','Basque':'eu','Belarusian':'be','Bengali':'bn','Bosnian':'bs','Bulgarian':'bg','Catalan':'ca','Cebuano':'ceb','Chinese':'zh-CN','Chinese2':'zh-TW','Corsican':'co','Croatian':'hr','Czech':'cs','Danish':'da','Dutch':'nl','English':'en','Esperanto':'eo','Estonian':'et','Finnish':'fi','French':'fr','Frisian':'fy','Galician':'gl','Georgian':'ka','German':'de','Greek':'el','Gujarati':'gu','Haitian Creole':'ht','Hausa':'ha','Hawaiian':'haw','Hebrew':'he','Hindi':'hi','Hmong':'hmn','Hungarian':'hu','Icelandic':'is','Igbo':'ig','Indonesian':'id','Irish':'ga','Italian':'it','Japanese':'ja','Javanese':'jv','Kannada':'kn','Kazakh':'kk','Khmer':'km','Kinyarwanda':'rw','Korean':'ko','Kurdish':'ku','Kyrgyz':'ky','Lao':'lo','Latin':'la','Latvian':'lv','Lithuanian':'lt','Luxembourgish':'lb','Macedonian':'mk','Malagasy':'mg','Malay':'ms','Malayalam':'ml','Maltese':'mt','Maori':'mi','Marathi':'mr','Mongolian':'mn','Myanmar (Burmese)':'my','Nepali':'ne','Norwegian':'no','Nyanja':'ny','Odia':'or','Pashto':'ps','Persian':'fa','Polish':'pl','Portuguese':'pt','Punjabi':'pa','Romanian':'ro','Russian':'ru','Samoan':'sm','Scots Gaelic':'gd','Serbian':'sr','Sesotho':'st','Shona':'sn','Sindhi':'sd','Sinhala (Sinhalese)':'si','Slovak':'sk','Slovenian':'sl','Somali':'so','Spanish':'es','Sundanese':'su','Swahili':'sw','Swedish':'sv','Tagalog (Filipino)':'tl','Tajik':'tg','Tamil':'ta','Tatar':'tt','Telugu':'te','Thai':'th','Turkish':'tr','Turkmen':'tk','Ukrainian':'uk','Urdu':'ur','Uyghur':'ug','Uzbek':'uz','Vietnamese':'vi','Welsh':'cy','Xhosa':'xh','Yiddish':'yi','Yoruba':'yo','Zulu':'zu'}
    return langs.get(language)

async def translator_check(channel_id):
    c = bot_db.cursor()
    c.execute("""SELECT * FROM translator WHERE channelid = ?""",[channel_id])
    return c.fetchone()

async def random_id():
    random_id = ''.join((random.choice(string.hexdigits) for x in range(48)))
    return random_id

@bot.event
async def on_ready():
    # alert_cmd = alert(bot)
    # await alert_cmd.alert_user()
    if 'token.txt' in os.listdir('./'):
        with open('./token.txt','r') as file:
            tkn = file.read()
            music.refresh_token = str(tkn)
        method = 1
    else:
        global code
        code = input("Input Auth Code for Spotify ")
        method = 0
    try:
        while True:
            print("Getting Spotify Auth Token")
            await get_spotify_token(method)
            method = 1
            await asyncio.sleep(3500)
    except Exception as e :
        print(e)

class alert(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    async def alert_user(self):
        user = await self.bot.fetch_user(522105985343684629)
        while True:
            await self.send_message(user)
            await asyncio.sleep(10)


    async def send_message(self,user):
        async with aiohttp.ClientSession() as session:
            async with session.get(url="https://shopatsc.com/products/playstation5-digital-edition") as response:
                if response.status != 404:
                    await user.send(f"{user.mention} PS5 Stock at https://shopatsc.com/products/playstation5-digital-edition")
                else:
                    print("No Stocks right now")
                await session.close()    

class music(commands.Cog):

    refresh_token = None
    access_token = None

    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.command(name="register")
    async def music_guild_register(self,ctx):
        c = conn.cursor()
        if ctx.author.id == 562553902990491659:
            c.execute("""INSERT INTO guild (guild_id) VALUES(?)""",[ctx.guild.id])
            conn.commit()
            await ctx.send("Guild registered for music commands")
        else:
            owner = await self.bot.fetch_user(562553902990491659)
            buttons = []
            buttons.append(create_button(style=ButtonStyle.success,label='Yes'))
            buttons.append(create_button(style=ButtonStyle.danger,label='No'))
            action_row = create_actionrow(*buttons)
            await owner.send("Approve ?",components=[action_row])
            try:
                confirm_comp = await wait_for_component(bot, components=action_row,timeout=30)
                await confirm_comp.reply('Okay')
                answer = confirm_comp.component.get('label')
            except:
                answer = "No"
            if answer == 'Yes':
                c.execute("""INSERT INTO guild (guild_id) VALUES(?)""",[ctx.guild.id])
                conn.commit()
                await ctx.send("Guild approved for music commands")
            else:
                await ctx.send("Guild was not approved for music commands")

    async def check_guild(self, ctx):
        c = conn.cursor()
        c.execute("""SELECT * FROM guild WHERE guild_id = ?""",[ctx.message.guild.id])
        if c.fetchone() == None:
            return False
        else:
            return True

    async def voice_var(self,voice_channel):
        try:
            return voice
        except Exception as e:
            backup_voice = guild_queue.queue.get(voice_channel.guild.id)['voice']
            if guild_queue.queue.get(voice_channel.guild.id)['voice'] != None:
                try:
                    voice = guild_queue.queue.get(voice_channel.guild.id)['voice']
                    voice1 = await voice.move_to(voice_channel)
                except Exception as e :
                    
                    return backup_voice
                guild_queue.queue.get(voice_channel.guild.id)['voice'] = voice1
                return voice1
                

            else:
                return 

    async def server_voice(self,guild_id):
        voice_clients = self.bot.voice_clients
        voice = None
        for voices in voice_clients:
            if voices.guild.id == guild_id:
                voice = voices
                break

        return voice

    async def player(self,url,guild_id):
        voice = await music.server_voice(self,guild_id)
        
        FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': '-vn'}
        ydl_opts = {'format': 'bestaudio','cookiefile':'./cookie.txt'}
        with YoutubeDL(ydl_opts) as ydl:
            info = ydl.extract_info(url, download=False)
            URL = info['formats'][0]['url']
        voice.play(discord.FFmpegPCMAudio(URL,**FFMPEG_OPTIONS),after = lambda e: asyncio.run_coroutine_threadsafe(self.queue_manager(guild_id), bot.loop))
        voice.source = discord.PCMVolumeTransformer(voice.source)
        voice.source.volume = .69

    async def queue_manager(self,guild_id):
        queue = guild_queue.queue.get(guild_id)
        if queue != None:
            playing = queue.get('playing')
            loop = queue.get('loop')
            if loop == 'one' and playing != 0:
                musicData = queue.get(playing)
            else:
                musicData = queue.get(playing+1)
        else:
            loop = False
            musicData = None
        voice_connected = await self.server_voice(guild_id)
        if musicData != None and voice_connected:
            if loop != 'one':
                guild_queue.queue.get(guild_id)['playing'] = playing + 1
            data = musicData.data
            url = await url_extract(data[0])
            await song_embed(guild_id)
            guild_queue.queue.get(guild_id)['stopped'] = False
            await music.player(self,url,guild_id)
        else:            
            if loop == False or voice_connected == None:
                embed = Embed(title="No more songs in queue",description="Play a song with \nStacy play [name]",color=0xff0f37)
                embed.set_image(url="https://cdn.discordapp.com/attachments/645820165405278209/876938530587291659/black14.jpg")
                message = await server_message(guild_id)
                buttons = []
                emoji_list = ['⏮️','Clear','Play','Loop','⏭️']
                for emoji in emoji_list:
                    buttons.append(create_button(style=ButtonStyle.blue, label=f'{emoji}',disabled=True))
                action_row = create_actionrow(*buttons)
                await message.edit(embed=embed,components=[action_row])
                guild_queue.queue.get(guild_id)['first_command'] = False
                guild_queue.queue.get(guild_id)['playing'] = playing
                guild_queue.queue.get(guild_id)['stopped'] = True
                voice = await music.server_voice(self,guild_id)
                tries = 0
                while guild_queue.queue.get(guild_id)['stopped'] and tries < 3:
                    tries += 1
                    disconnect = False
                    for member in voice.channel.members:
                        if member.bot == False:
                            disconnect = False
                            break
                        else:
                            disconnect = True
                    if disconnect:
                        try:
                            del guild_queue.queue[guild_id]
                            if voice.is_playing() or voice.is_paused():
                                voice.stop()
                            if voice:
                                await voice.disconnect()
                                break
                        except Exception as e:
                            print(e)
                    await asyncio.sleep(40)
                if guild_queue.queue.get(guild_id)['stopped'] and tries >= 3:
                    try:
                        await voice.disconnect()
                        del guild_queue.queue[guild_id]
                    except:
                        del guild_queue.queue[guild_id]
            else:
                guild_queue.queue.get(guild_id)['playing'] = 0
                await music.queue_manager(self,guild_id)

    @commands.Cog.listener()
    async def on_message(self,message):
        try:
            content = message.content
            valid = await validator(message)
            if valid:
                await message.delete()
                if content.startswith('http'):
                    data = await url_sorter(content,message.guild.id,message.author.name)
                else:
                    data = await YouTube_Searcher(content)
                if data != None:
                    guild_queue(message.guild.id,message.author.name,data)
                    await song_embed(message.guild.id)
        except:
            pass

    async def reaction_events(self,ctx):
        try:
            voice = await music.server_voice(self,ctx.guild_id)
        except Exception as e:
            print(e)
        try:
            if ctx.author.voice == None:
                await ctx.author.send("Don't Spoil fun, Join VC instead. dum dum.")
                return
            component = ctx.component
            opt = component.get('label')
            if opt == 'Play' or opt == 'Pause':
                if voice and voice.is_connected() and voice.is_paused():
                    voice.resume()
                    await song_embed(ctx.guild_id,"Playing")
                elif voice and voice.is_connected() and voice.is_playing():
                    voice.pause()
                    await song_embed(ctx.guild_id,"Paused")
                else:
                    await music.queue_manager(self,ctx.guild_id)

            elif opt == 'Clear':
                if voice and voice.is_connected():
                    try:
                        del guild_queue.queue[ctx.guild_id]
                        if voice.is_playing() or voice.is_paused():
                            voice.stop()
                        guild = await bot.fetch_guild(ctx.guild_id)
                        if (guild.voice_client):
                            await guild.voice_client.disconnect()
                    except Exception as e:
                        print(e)

            elif opt == '⏮️':
                playing = guild_queue.queue.get(ctx.guild_id).get('playing')
                if playing > 1:
                    guild_queue.queue.get(ctx.guild_id)['playing'] = playing -2
                    voice.stop()

            elif opt == '⏭️':
                if voice and voice.is_connected() and voice.is_playing() or voice.is_paused():
                    voice.stop()
            elif opt == 'Loop':
                loop = guild_queue.queue.get(ctx.guild_id).get('loop')
                if loop == False:
                    guild_queue.queue.get(ctx.guild_id)['loop'] = True
                elif loop == True:
                    guild_queue.queue.get(ctx.guild_id)['loop'] = 'one'
                elif loop == 'one':
                    guild_queue.queue.get(ctx.guild_id)['loop'] = False
                await song_embed(ctx.guild_id)
            
        except Exception as e:
            print(e)

    @commands.Cog.listener()
    async def on_component(self,ctx: ComponentContext):
        gid = ctx.guild_id
        message_id = await get_message_id(gid)
        message = ctx.origin_message
        
        if message_id == message.id and guild_queue.queue.get(gid) != None:
            reply = await ctx.reply(f'Processing... {ctx.author}')            
            await reply.delete()
            await music.reaction_events(self,ctx)

    @commands.command(name="bind")
    async def channel_binder(self,ctx):
        embed = Embed(description="Binding channel",color=0xfcff61)
        message = await ctx.send(embed=embed)
        buttons = []
        emoji_list = ['⏮️','Clear','Play','Loop','⏭️']
        for emoji in emoji_list:
            buttons.append(create_button(style=ButtonStyle.blue, label=f'{emoji}'))
        action_row = create_actionrow(*buttons)
        await register_channel(ctx,message.id)
        embed = Embed(description="Channel binded",color=0x3bff6f)
        
        await message.edit(embed=embed, components=[action_row])
  
    @commands.command(name="play")
    async def play(self,ctx,*,what=None):
        await ctx.message.delete()
        guild_id = ctx.message.guild.id
        guild_id_perm = await self.check_guild(ctx)
        if guild_id_perm == False:
            await ctx.send("Voice commands are limited to specific servers")
            return
        async def play_valid():
            channel_id = await get_channel_id(ctx.guild.id)
            if channel_id == None:
                await ctx.send(embed=Embed(description='Use `stacy bind` before using play command',color=0x00ffbb))
                return False
            else:
                if channel_id != ctx.message.channel.id:
                    await ctx.send(embed=Embed(description=f'Command restricted to <#{channel_id}>',color=0x00ffbb))
                    return False
                else:
                    return True
        valid = await play_valid()
        if valid != True:
            return

        
        voice_channel = ctx.author.voice.channel
        if voice_channel == None:
            await ctx.send(embed=Embed(description="Join a Voice Channel to execute this command",color=0xff0000))
            return
        
        try:
            voice = await voice_channel.connect()
        except:
            voice = await music.server_voice(self,guild_id)
            
        if what is None:
            embed = Embed(description='Provide a name/link for song',color=0xfcff61)
            await ctx.send(embed=embed)
            return

        
        if what.startswith('http'):
            data = await url_sorter(what,ctx.guild.id,ctx.author.name)
        else:
            data = await YouTube_Searcher(what)
        if data == None:
            return
        guild_queue(guild_id,ctx.author.name,data)
        
        if guild_queue.queue.get(ctx.guild.id)['first_command'] == None:
                guild_queue.queue.get(ctx.guild.id)['first_command'] = True
        playing = guild_queue.queue.get(guild_id).get('playing')
        try:
            if guild_queue.queue.get(guild_id)['stopped']:
                await music.queue_manager(self,guild_id)
            else:    
                await song_embed(guild_id)
        except:
            await music.queue_manager(self,guild_id)
            
        if what.find('open.spotify.com/playlist') != -1 and what.startswith('http'):
            add_thread = threading.Thread(target=asyncio.run, args=[url_sorter(what,ctx.guild.id,ctx.author.name,True)])
            add_thread.daemon = True
            add_thread.start()

    @commands.command(name="search",aliases=["s","srch"])
    async def search(self,ctx,*,what):
        guild_id_perm = await self.check_guild(ctx)
        if guild_id_perm == False:
            await ctx.send("Voice commands are limited to specific servers")
            return

        await ctx.message.delete()
        async def play_valid():
            channel_id = await get_channel_id(ctx.guild.id)
            if channel_id == None:
                await ctx.send(embed=Embed(description='Use `stacy bind` before using play command',color=0x00ffbb))
                return False
            else:
                if channel_id != ctx.message.channel.id:
                    await ctx.send(embed=Embed(description=f'Command restricted to <#{channel_id}>',color=0x00ffbb))
                    return False
                else:
                    return True
        valid = await play_valid()
        if valid != True:
            return


        voice_channel = ctx.author.voice.channel
        if voice_channel == None:
            await ctx.send(embed=Embed(description="Join a Voice Channel to execute this command",color=0xff0000))
            return
        
        try:
            voice = await voice_channel.connect()
        except:
            voice = await music.server_voice(self,ctx.guild.id)

        results = await YouTube_Searcher(what,5)
        embed = Embed(title=f'Search results for {what}',color=0xff2445)
        embed.set_thumbnail(url=f"{results[0].get('thumbnails')[-1]}")
        buttons = []
        custom_ids = {}
        index = 0
        emoji = {0:'1️⃣',1:'2️⃣',2:'3️⃣',3:'4️⃣',4:'5️⃣'}
        for opts in emoji:
            uid = await random_id()
            buttons.append(create_button(style=ButtonStyle.grey, label=emoji.get(opts),custom_id=uid))
            custom_ids[uid] = opts
        
        
        for result in results:
            index += 1
            embed.add_field(name="Name",value=result.get('title'),inline=False)
            embed.add_field(name="Index",value=index,inline=True)
            embed.add_field(name="Duration",value=result.get('duration'),inline=True)
            embed.add_field(name="Channel",value=f"{result.get('channel')}",inline=True)
        embed.set_footer(text="React to the index you'd like to play")
        action_row = create_actionrow(*buttons)

                
        try:
            message = await ctx.send(embed=embed,components=[action_row])
            confirm = await wait_for_component(bot, components=action_row,timeout=30)
            
            answer = confirm.component
            
            index = custom_ids.get(answer.get('custom_id'))
        except Exception as e:
            print(e)
            index = None

        await message.delete()
        if index != None:
            data = results[index]
            guild_queue(ctx.guild.id,ctx.author.name,[data])
            playing = guild_queue.queue.get(ctx.guild.id).get('playing')
            if guild_queue.queue.get(ctx.guild.id)['first_command'] == None:
                guild_queue.queue.get(ctx.guild.id)['first_command'] = True
            if playing == 0 or guild_queue.queue.get(ctx.guild.id)['first_command'] == False:
                await music.queue_manager(self,ctx.guild.id)
            else:
                await song_embed(ctx.guild.id)

    @commands.command(name="clear")
    async def clear(self,ctx, amount=5):
        if ctx.author.id == 562553902990491659:
	        await ctx.channel.purge(limit=amount)
    

class misc(commands.Cog):
    
    def __init__(self, bot):
        self.bot = bot
        self._last_member = None

    @commands.command(name="spam")
    async def spam(self,ctx,member:discord.Member=None,*,text=None):
        if ctx.guild.id == 645813589029945365:
            return
        def is_int(what):
            try:
                int(what)
                return True
            except:
                return False
        num = 5
        if text != None:
            if is_int(text[-1]):
                num = int(text[-1])
                text = text[:-1]
            spamText = ''.join(x for x in text)
        else:
            spamText = 'you have been spammed'
        spam_messages = {}
        for spam in range(num):
            spam_messages[spam] = await ctx.send(f'{member.mention} {spamText}')
        try:
            def check(m):
                return m.author.id == member.id and m.channel.id == ctx.message.channel.id
            await bot.wait_for('message',check=check,timeout=120)
            for message in spam_messages:
                await spam_messages.get(message).delete()
        except:
            for message in spam_messages:
                if message != 0:
                    await spam_messages.get(message).delete()
                else:
                    embed = Embed(description=f'{member.mention} you were spammed by {ctx.author.mention}',color=0xffffff)
                    if spamText != 'you have been spammed':
                        embed.add_field(name='Spam message',value=spamText)
                    await spam_messages.get(message).edit(embed=embed,content=" ")
    
    @commands.command(name="ping")
    async def ping(self,ctx):
        await ctx.send(f'PONG! {round(bot.latency*1000,1)} ms')

    @commands.Cog.listener()
    async def on_message(self,message):
        if message.author.bot == False:
            change = False
            if message.content.find('https://media.discordapp.net/') != -1:
                new_message = message.content
                while new_message.find('https://media.discordapp.net/') != -1: 
                    link = None
                    for messages in new_message.split():
                        if messages.startswith('https://media.discordapp.net/') and messages.split('.')[-1] in ['mp4', 'webm' ,'mov']:
                            link = messages
                            change = True
                            break
                    if link != None:
                        link = link.replace('https://media.discordapp.net/','https://cdn.discordapp.com/')
                        new_message = new_message.replace(messages,link)
                    else:
                        break
            if change:
                await message.channel.send(f"{message.author.mention} : {new_message}")
                await message.delete()

    # @cog_ext.cog_slash(name="avatar",
    # description="Get avatar image of a person.",
    # type_request=2,
    # guild_ids=[929305041108619294])
    # async def avatar_image(self, ctx):
    #     print(ctx)
        

class custom_emojis(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self.last_user = None
    
    @commands.Cog.listener()
    async def on_message(self,message):
        is_emoji = await self.fetch_emoji(message.author,message.content)
        if is_emoji != None:
            await self.send_webhook(message,is_emoji[-1])

    async def send_webhook(self, ctx, emoji_url):
        textChannel = ctx.channel
        url = str(ctx.author.avatar_url)
        image = requests.get(url).content
        if ctx.author.nick != None:
            nameUsed = ctx.author.nick
        else:
            nameUsed = ctx.author.name
        newWebhook = await textChannel.create_webhook(name=nameUsed, avatar=image, reason="To send a custom emoji")
        await ctx.delete()
        await newWebhook.send(emoji_url)
        await newWebhook.delete()

    async def register_emoji(self,ctx,name,url):
        c = bot_db.cursor()
        existing = c.execute("""SELECT * FROM emojis WHERE userid = ? AND name = ?""",[ctx.author.id, name]).fetchone()
        if existing != None:
            c.execute("""DELETE FROM emojis WHERE userid = ? AND name = ?""",[ctx.author.id, name])
        
        c.execute("""INSERT INTO emojis (userid, name , url) VALUES (?,?,?)""",[ctx.author.id, name , url])
        bot_db.commit()

    async def fetch_emoji(self,author,name):
        c = bot_db.cursor()
        existing = c.execute("""SELECT * FROM emojis WHERE userid = ? AND name = ?""",[author.id, name]).fetchone()
        return existing

    @cog_ext.cog_slash(name="list",
    description="List your custom emojis.",
    options=[create_option(name="url",description="if you want the name for specific url.",option_type=3,required=False)])
    async def list_custom_emoji(self,ctx,url=None):
        c = bot_db.cursor()
        if url == None:
            listing = c.execute("""SELECT * FROM emojis WHERE userid = ?""",[ctx.author.id]).fetchall()
            print(listing)
        else:
            listing = c.execute("""SELECT * FROM emojis WHERE userid = ? and url = ?""",[ctx.author.id,url]).fetchall()
            print(listing)
        name = {}
        for names in listing:
            name[names[2]] = names[3]
        index = 0
        message = ""
        sub_index = 0
        for emojis in name:
            index += 1
            sub_index += 1
            message += f"""{index} {emojis} : {name.get(emojis)}\n"""
            if sub_index > 5:
                await ctx.author.send(message)    
                sub_index = 0
                added = True
                message = ""
                continue
            added = False
        if added == False:
            await ctx.author.send(message)
        await ctx.send("Sent the listings in DMs")



    @cog_ext.cog_slash(name="emoji",
    description="Register a custom emoji.",
    options=[
        create_option(
            name="name",
            description="Name of emoji",
            required=True,
            option_type=3,
            choices = []
        ),
        create_option(
        name="url",
        description="URL to emoji.",
        required=True,
        option_type=3,
        choices = []
        ),
    ])
    async def custom_emoji(self,ctx,name,url):
        if url.startswith("http"):
            try:
                await self.register_emoji(ctx,name,url)
                await ctx.send("Emoji registered.")
            except:
                await ctx.send("Some error occurred while registering your request.")
        else:
            await ctx.send("Please provide a valid URL.")

class translator(commands.Cog):

    def __init__(self, bot):
        self.bot = bot
        self._last_member = None
    
    @commands.Cog.listener()
    async def on_message(self,message):
        if message.author.bot:
            return
        language_code = await translator_check(message.channel.id)
        if language_code != None:
            language_code = language_code[-1]

            translation = t.translate(message.content, dest=language_code)
            if translation.src != translation.dest:
                try:
                    await message.channel.send(f"`{message.author}` :{message.content}\n\n`Translation:` {translation.text}\n\n`Pronunciation:` {translation.pronunciation}\n--------------------------------\n")
                    await message.delete()
                except Exception as e:
                    print(e)

    @commands.command(name="translator")
    async def translating_register(self,ctx,language = None):
        if language == None:
            await ctx.send(embed=Embed(description='Please specify a language',color=0xff0000))
            return
        language_code = await language_validator(language)
        if language_code == None:
            await ctx.send(embed=Embed(description='Please specify a language that humans know.',color=0xff0000))
            return
        else:
            await register_translator(ctx,language)

    @commands.command(name="translator_stop")
    async def translating_deregister(self,ctx):
        c = bot_db.cursor()
        c.execute("""SELECT * FROM translator WHERE channelid = ?""",[ctx.message.channel.id])
        present = c.fetchone()
        if present != None:
            c.execute("""DELETE FROM translator WHERE channelid = ?""",[ctx.message.channel.id])
            bot_db.commit()
            await ctx.send(embed=Embed(description="Translation stopped for this channel",color=0xffff00))    

class reactions(commands.Cog):

    list_of_all_roleplay = ['die', 'blushcry', 'deredere', 'kill', 'punch', 'cuddle', 'tickle', 'boop', 'pout', 'happy', 'triggered', 'lick', 'nom', 'hold', 'shoot', 'teehee', 'bite', 'scoff', 'facepalm', 'greet', 'lewd', 'shy', 'slap', 'bully', 'purr', 'sad', 'sleep', 'stare', 'glare', 'laugh', 'kiss', 'wave', 'poke', 'run', 'shrug', 'smug', 'highfive', 'pat', 'wiggle', 'thinking', 'thumbsup', 'handholding', 'hug', 'titties', 'dance', 'thonking', 'stomp', 'clap', 'wag', 'smile', 'grin', 'cry', 'blowjob', 'blush', 'snuggle']


    async def get_random_reaction(self,reaction):
        c = bot_db.cursor()
        c.execute("""SELECT * FROM roleplay WHERE reaction = ?""",[reaction])
        gifs = c.fetchall()
        random_image = random.choice(gifs)
        url = random_image[-1]
        return url

    @commands.Cog.listener()
    async def on_message(self,message):

        message_content = message.content
        for bad_chars in ['!','@','#','$','%','^','&','(',')','-','_','=','+','[','{',']','}',';',':','"',"'",',','<','.','>','\\','|','?','/','`','~']:
            message_content = message_content.replace(bad_chars,' ')
        message_content = message_content.split(' ')
        

        if len(message_content) < 50:
            reaction = None
            for word in message_content:
                if word.lower() in reactions.list_of_all_roleplay:
                    reaction = word.lower()
                    break
            if reaction != None:
                url = await reactions.get_random_reaction(self,reaction)

                history = await message.channel.history(limit=2).flatten()
                try:
                    user_obj = history[-1].author
                except:
                    user_obj = None
                replies = {'die':'dies with', 'blushcry':'cries infront of', 'deredere':'adores', 'kill':'kills', 'punch':'punches', 'cuddle':'cuddles', 'tickle':'tickles', 'boop':'boops', 'pout':'pouts infront of', 'happy':'is happy with', 'triggered':'was triggered by', 'lick':'licks', 'nom':'noms', 'hold':'holds', 'shoot':'shoots', 'teehee':'*does teehee to*', 'bite':'bites', 'scoff':'scoffs to', 'facepalm':'facepalms on act of', 'greet':'greets', 'lewd':'is getting lewd with', 'shy':'...', 'slap':'slaps', 'bully':'bullies', 'purr':'purrs', 'sad':'is sad with', 'sleep':'sleeps with', 'stare':'satres', 'glare':'glares', 'laugh':'laughs on', 'kiss':'kisses', 'wave':'waves to', 'poke':'pokes', 'run':'runs away from', 'shrug':'shrugs to', 'smug':'sumgs to', 'highfive':'highfives', 'pat':'pats', 'wiggle':'wiggles for', 'thinking':'is think about it, wait a min', 'thumbsup':'cheers', 'handholding':'hold hands with', 'hug':'hugges', 'titties':'sees your titties,', 'dance':'dances with', 'thonking':'thonks about it , wait a min', 'stomp':'stomps on', 'clap':'claps for ', 'wag':'wags for', 'smile':'smiles towards', 'grin':'grin towards', 'cry':'cries infront of', 'blowjob':'gives a blowjob to', 'blush':'blushes infont of', 'snuggle':'snuggles'}
                if user_obj != None:
                    reply = replies.get(reaction)
                    embed = Embed(description=f"{message.author.mention} {reply} {user_obj.mention}",color=0xff003c)
                    embed.set_image(url=url)
                else:
                    embed = Embed(description=f"<@772381863528103966> {message.author.mention}",color=0xff003c)
                    embed.set_image(url=url)
                await message.reply(embed=embed)

class game(commands.Cog):
    
    def __init__(self,bot):
        self.bot = bot
        self.last_user = None
    
    @commands.command(name="akinator")
    async def akinator_game(self,ctx):
        msg = await ctx.send("Getting Akinator")

        z = 1
        aki = akinator.Akinator()
        embed = Embed(title="Akinator",description="Guess A person and I'll tell you who are you thinking of",color=0xffffff)
        embed.add_field(name="Progress",value="Progress is 0%",inline=False)
        embed.add_field(name="Question",value=f"Q{z}: {aki.start_game()}")
        embed.set_footer(text="Reply with: ['Yes','No','Idk'(I don't know),'p'(Probably),'pn'(Probably Not)]")
        
        game = await ctx.send(embed=embed)
        await msg.delete()
        lis = []
        replies = {'Yes':'y','No':'n','Probably':'p','Probably Not':'pn',"I don't know":'idk'}
        while aki.progression <= 95:
            z += 1
            try:
                buttons = []
                custom_ids = {}
                for opts in replies:
                    uid = await random_id()
                    buttons.append(create_button(style=ButtonStyle.primary, label=opts, custom_id=uid))
                    custom_ids[uid] = replies.get(opts)
                action_row = create_actionrow(*buttons)
                await game.edit(components=[action_row])
                click = await wait_for_component(bot, components=action_row,timeout=30)
                
                component = click.component
                reply = custom_ids.get(component.get('custom_id'))
                temp = await click.reply(f"{component.get('label')}, you say...")
                await temp.delete()
            except Exception as e:
                print(e)
                await ctx.send("Timed out")
                return

            
            lis.append(aki.answer(reply))
            embed = Embed(title="Akinator",description="Guess A person and I'll tell you who are you thinking of",color=0xffffff)
            embed.add_field(name="Progress",value=f"Progress is {aki.progression}%",inline=False)
            embed.add_field(name="Question",value=f"Q{z}: {lis[-1]}")
            embed.set_footer(text="Reply with: ['Yes','No','Idk'(I don't know),'p'(Probably),'pn'(Probably Not)]")
            await game.edit(embed=embed)
        aki.win()
        yes_uid = await random_id()
        final_buttons = [create_button(style=ButtonStyle.green,label='Yes',custom_id=yes_uid),create_button(style=ButtonStyle.red,label='No')]
        action_row = create_actionrow(*final_buttons)
        embed = Embed(title="Akinator",description="Guess A person and I'll tell you who are you thinking of",color=0xffffff)
        embed.add_field(name="Progress",value=f"Progress is {aki.progression}%",inline=False)
        embed.add_field(name="Question",value=f"Q{z}: {lis[-2]}",inline=False)
        embed.add_field(name="You were thinking of",value=f"{aki.first_guess['name']}",inline=True)
        embed.add_field(name="Origin",value=f"{aki.first_guess['description']}")
        embed.set_image(url=f"{aki.first_guess['absolute_picture_path']}")
        embed.set_footer(text="Was I correct? Yes/No")
        await game.edit(embed=embed,components=[action_row])
        try:
            correct = await wait_for_component(bot, components=action_row,timeout=30)
            if correct.component.get('custom_id') == yes_uid:
                ans = 'y'
            else:
                ans = 'n'
            await correct.defer(edit_origin=True)
        except asyncio.TimeoutError:
            embed.set_footer(text="I'm going to Assume it was correct")
            ans = "y"

        if ans.lower() == "yes" or ans.lower() == "y":
            embed = Embed(title="Akinator",description="Guess A person and I'll tell you who are you thinking of",color=0x66ff33)
            embed.add_field(name="Progress",value=f"Progress is 100.00%",inline=False)
            embed.add_field(name="Question",value=f"Q{z}: {lis[-2]}",inline=False)
            embed.add_field(name="You were thinking of",value=f"{aki.first_guess['name']}",inline=True)
            embed.add_field(name="Origin",value=f"{aki.first_guess['description']}")
            embed.set_image(url=f"{aki.first_guess['absolute_picture_path']}")
            embed.set_footer(text="Yes.! Nailed it. Wannna Play again?")
        else:
            embed.set_footer(text="Oh man.! Who were you thinking of by the way?")
        try:
            await correct.reply(embed=embed,components=None)
        except:
            await game.edit(embed=embed, components=None)

class dev(commands.Cog):
    
    def __init__(self,bot):
        self.bot = bot
        self.last_user = None
    
    @commands.Cog.listener()
    async def on_message(self,message):
        if users.added_users.get(message.author.id) != None:
            if message.guild.id in users.added_users.get(message.author.id):
                await message.delete()

    @commands.command(name="godunmute")
    async def gum(self,ctx,user:discord.Member):
        if ctx.author.id != 307545614512685056:
            await ctx.send("This command can only be initiated by भीष्म पितामह.")
            return
        if users.added_users.get(user.id) != None:
            if ctx.guild.id in users.added_users.get(user.id):
                temp = users.added_users.get(user.id)
                temp2 = []
                for x in temp:
                    if x != ctx.guild.id:
                        temp2.append(x)
                users.added_users[user.id] = temp2

        await ctx.send(f"unmuted {user.mention}")

    @commands.command(name="godmute")
    async def gm(self,ctx,user:discord.Member):
        if ctx.author.id != 307545614512685056:
            await ctx.send("This command can only be initiated by भीष्म पितामह.")
            return
        await ctx.message.delete()
        if users.added_users.get(user.id) == None:
            users.added_users[user.id] = []
        users.added_users.get(user.id).append(ctx.guild.id)
        await ctx.author.send(f'God muted {user.name}')
        
        

    @commands.command(name="mute")
    async def mute(self,ctx,user:discord.Member,time="1m"):
        pass

class admin(commands.Cog):

    def __init__(self,bot):
        self.bot = bot
        self.last_user = None
    
    @cog_ext.cog_slash(name="timeout",
    description="Timeout a person.",
    options=[
        create_option(
            name="member",
            description="The person you'd like to timeout",
            required=True,
            option_type=6,
            choices = []
        ),
        create_option(
        name="duration",
        description="Duration, only numbers.",
        required=True,
        option_type=3,
        choices = []
        ),
        create_option(
            name="time_format",
            description="Seconds, Minutes or Hours?",
            required=True,
            option_type=3,
            choices = ["secs","mins","hours"]
        ),
        create_option(name="reason",
        description="I mean , why dude?",
        required=False,
        option_type=3,
        choices = [])
    ])
    async def timeout(self, ctx, member: discord.Member = None, duration = None, time_format=None , reason="For some personal reason"):
        till = duration
        time = time_format
        if member == None or till == None or time == None:
            await ctx.send(f"Command example \n`stacy timeout @{ctx.author} 2 hour/mins/secs")
            return
        if not ctx.author.guild_permissions.manage_guild:
            await ctx.send("You don't have the permissions to use this command.")
            return
        try:
            till = int(till)
        except:
            await ctx.send(f"Enter a time only as integer :)")
            return
        
        header = {"Authorization": f"Bot {self.bot.http.token}", "X-Audit-Log-Reason":reason}
        url = f"https://discord.com/api/v9/guilds/{ctx.guild.id}/members/{member.id}"
        time = time.lower()
        till_copy = till
        if time == "days" or time == "day":
            time = "days"
            till = till * 86400
        elif time == "hours" or time == "hour" or time == "hrs" or time == "hr" or time == "h":
            time = "hours"
            till = till * 3600
        elif time == "minutes" or time == "minute" or time == "mins" or time == "min" or time == "m":
            time = "minute"
            till = till * 60
        elif time == "seconds" or time == "second" or time == "secs" or time == "sec" or time == "s":
            time = "seconds"
        else:
            await ctx.send("Give a valid argument!")
            return
        if till > 2419200:
            await ctx.send("you can timeout someone upto 28 days only.")
            return
        tilltime = (datetime.datetime.utcnow() + datetime.timedelta(seconds=till)).isoformat()
        json = {'communication_disabled_until': tilltime}
        
        async with aiohttp.ClientSession(timeout=aiohttp.ClientTimeout(10)) as sess:
            async with sess.patch(url, json=json, headers=header) as session:
                if session.status in range(200, 299):
                    embed = discord.Embed(title="Timed out ✅",
                                          description=f"Successfully time out {member.name} for {till_copy} {time}.",
                                          color=0xf52f25)
                    await ctx.send(embed=embed)
                    await sess.close()


    @commands.command(name="kick")
    async def kick(self,ctx,member:discord.Member=None,*,reason=None):
        if reason == None:
            await ctx.send(f'Please provide a reason to kick {member.mention}')
            return
        if ctx.author.guild_permissions.kick_members or ctx.author.guild_permissions.administrator:
            embed = Embed(title=f"You've been *kicked* from {ctx.guild}",color=0xff003c)
            embed.add_field(name="Reason",value=f'{reason}')
            embed.set_footer(text="You can rejoin , just behave this time.",icon_url=ctx.guild.icon_url)
            try:
                await member.send(embed=embed)
            except:
                pass
            await member.kick(reason=reason)
            await ctx.send(embed=Embed(description=f'{member.name} has been kicked from {ctx.guild}.',color=0xff003c))
        else:
            await ctx.send(f'{ctx.author.mention} why are you gae?')
    
    @commands.command(name="ban")
    async def ban(self,ctx,member:discord.Member=None,*,reason=None):
        if reason == None:
            await ctx.send(f'Please provide a reason to ban {member.mention}')
            return
        if ctx.author.guild_permissions.ban_members or ctx.author.guild_permissions.administrator:
            embed = Embed(title=f"You've been *banned* from {ctx.guild}",color=0xff003c)
            embed.add_field(name="Reason",value=f'{reason}')
            embed.set_footer(text="There's no hope left for you to get back in.",icon_url=ctx.guild.icon_url)
            try:
                await member.send(embed=embed)
            except:
                pass
            await member.ban(reason=reason)
            await ctx.send(embed=Embed(description=f'{member.name} has been banned from {ctx.guild}.',color=0xff003c))
        else:
            await ctx.send(f'{ctx.author.mention} why are you gae?')

class YouTube_Class(commands.Cog):

    def __init__(self,bot):
        self.bot = bot
        self.last_user = None

    @cog_ext.cog_slash(name="youtube",
    guild_ids=[929305041108619294],
    description="Download a youtube media from link.",
    options= [ create_option(name="link",
        description="Post's link",
        required=True,
        option_type=3,
        choices = [])
    ])
    async def YouTube_cmd(self,ctx,link):
        await ctx.send("wait")
        res = await YouTube_Searcher(link)
        print(res)
        ydl_opts = {'format': 'bestaudio','cookiefile':'./cookie.txt'}
        with YoutubeDL(ydl_opts) as ydl:
            info = ydl.extract_info(link, download=False)
            
            

def setup():
    # bot.add_cog(alert(bot))
    bot.add_cog(misc(bot))
    bot.add_cog(admin(bot))
    bot.add_cog(music(bot))
    bot.add_cog(translator(bot))
    bot.add_cog(dev(bot))
    bot.add_cog(game(bot))
    bot.add_cog(custom_emojis(bot))
    # bot.add_cog(YouTube_Class(bot))
    

setup()
