from youtube_search import YoutubeSearch
import json

results = YoutubeSearch('https://www.youtube.com/watch?v=0opZqh_TprM&list=PLx0sYbCqOb8Q_CLZC2BdBSKEEB59BOPUM&ab_channel=LyricalLemonade', max_results=1).to_json()

print(results)
print(f"{json.loads(results).get('videos')[0].get('title')}")
print(f"https://www.youtube.com{json.loads(results).get('videos')[0].get('url_suffix')}")
