#!/bin/python3
import ast
import asyncio
import json
import os
import random
import shutil
import sys
import traceback
from re import search
import aiohttp
import akinator
import discord
import ffmpeg
import requests
import rule34
import wikipediaapi
from akinator.async_aki import Akinator
from asyncpraw import Reddit
from asyncprawcore import Requestor
from discord import Embed, FFmpegOpusAudio
from discord.ext import commands
from discord.utils import get
from googletrans import Translator
from youtube_dl import YoutubeDL
from youtube_search import YoutubeSearch

from database import (add, buggy, call, cbal, fbal, ftokn, gender, gse, scall,
                      suges, use)
from meme_gen import memegen
from passwd_dict import dict_call  # create a file with dictionary and function
from reb import reb
from rps import gam
from search import srch
from urban import udict
from yoda import yodish

reddit = Reddit(client_id=f"{dict_call('Reddit').get('client_id')}",
                    client_secret=f"{dict_call('Reddit').get('client_secret')}",
                    username=f"{dict_call('Reddit').get('username')}",
                    password=f"{dict_call('Reddit').get('password')}",
                    user_agent=f"{dict_call('Reddit').get('user_agent')}"
                    )

subreds = ['memes','dankmemes']
token=f"{dict_call('token')}"
Role = "Member"
intents = discord.Intents.all()
intents.members = True
bot = commands.Bot(command_prefix=commands.when_mentioned_or("stacy ","Stacy ","stacy","Stacy",'Stacy','sTacy','stAcy','staCy','stacY','STacy','STacy','STAcy','STaCy','STacY','STAcy','STAcy','STAcy','STACy','STAcY','STACy','STACy','STACy','STACy','STACY','STACY','STACY','STACY','STACY','STACY','StAcy','STAcy','StAcy','StACy','StAcY','StACy','STACy','StACy','StACy','StACY'), case_insensitive=True, guild_subscriptions=True, intents=intents)
bot.remove_command('help')

translator = Translator()
rule34 = rule34.Rule34()

async def fetch():
    lsub = random.choice(subreds)
    all_subs = []
    subreddit = await reddit.subreddit(f"{lsub}")
    async for new in subreddit.top(limit=69):
            all_subs.append(new)
    
    breaker = 0
    num = 0
    while breaker== 0:
        try:
            sub = random.choice(all_subs)
            url = sub.url
            if (url.startswith("https://i.redd.it") or url.startswith("https://i.imgur")) and (url.endswith(".jpg") or url.endswith(".png") or url.endswith(".jpeg") or url.endswith(".gif")) :
                title = sub.title
                breaker = 1
            if num <= 69:
                num+=1
            else:
                breaker = 1
        except:
            pass

    return [url,title,subreddit]

def unicod(k):
    i = k.encode('utf-8')
    if len(str(i)) >= 14:
        return i
    else:
        return k

def is_owner(ctx):
    if ctx.author.id == 562553902990491659:
        return True
    else:
        return False

def color():
    colors = [0xFF355E,0xFD5B78,0xFF6037,0xFF9966,0xFF9933,0xFFCC33,0xFFFF66,0xFFFF66,0xCCFF00,0x66FF66,0xAAF0D1,0x50BFE6,0xFF6EFF,0xEE34D2,0xFF00CC,0xFF00CC,0xFF3855,0xFD3A4A,0xFB4D46,0xFA5B3D,0xFFAA1D,0xFFF700,0x299617,0xA7F432,0x2243B6,0x5DADEC,0x5946B2,0x9C51B6,0xA83731,0xAF6E4D,0xBFAFB2,0xFF5470,0xFFDB00,0xFF7A00,0xFDFF00,0x87FF2A,0x0048BA,0xFF007C,0xE936A7]
    return random.choice(colors)

def ebed(title):
    embed = Embed(title=f"{title}",color=color())
    return embed
    
def medit(inp):
    k = inp
    k = str(k).replace('<','{')
    k = k.replace('=',':')
    k = k.replace('>','}')
    o = int(len(k)) - 1
    k = k[29:o]
    k = k.replace('Member id',"'Member id'")
    k = k.replace('name',",'name'")
    k = k.replace('discriminator',",'discriminator'")
    k = k.replace('bot',",'bot'")
    k = k.replace('nick',",'nick'")
    k = k.replace('guild',",'guild'")
    k = k.replace('Guild id',"'Guild id'")
    k = k.replace('shard_id',",'shard_id'")
    k = k.replace('chunked',",'chunked'")
    k = k.replace('member_count',",'member_count'")
    k = (ast.literal_eval(k))
    return k

async def activities():
    while True:
        act = ["Watching","Playing"]
        if random.choice(act) == "Playing":
            listOfActivites = ["with 51 Commands","Cyberpunk 2078","GTA V in Strip Club RN","with my MP5","Lovefool on Spotify","Deteroit become human","with my Scar","Countdown with the quite kid (He seems serious)","with Troy","with Monke","with him"]
            activity = random.choice(listOfActivites)
            await bot.change_presence(activity=discord.Game(name=activity))
        else:
            listOfActivites = ["Tejansh studying for his boards","Divik's Pasta Code","#","All possible outcomes of 2021","Madara vs Hashirama","Konoha","Naruto vs Pain","Madara vs 5 Kage","Itachi","ZeroTwo with Hiro","Ichigo","Abhay fixing me","Madara whooping Shinobi Aliance's Ass","Kakashi's hand signs","Quite kid pulling out his MP5 (ttyl)","Mai-san","Teju Senpai","Kenma","Fushiguro"]
            activity = random.choice(listOfActivites)
            await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name=activity))
        await asyncio.sleep(60)

async def vc(ctx):
    global voice
    channel = ctx.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)
    if voice and voice.is_connected:
        await voice.move_to(channel)
    else:
        voice = await channel.connect()
    
    return voice

@bot.event
async def on_ready():
    print("RUNNING")
    
    await bot.change_presence(activity=discord.Game(name="With Steve"))

@bot.command(name='help')
async def hlp(ctx, *,cat=None):
    if cat is None:
        embed = Embed(title="List of Commands Category",description="Displaying list of all commands",color=color())
        embed.add_field(name='Misc :alien:',value='stacy help misc', inline=True)
        embed.add_field(name='Fun :joy:',value='stacy help fun',inline=True)
        embed.add_field(name='Music :musical_note: ', value='stacy help music',inline=True)
        embed.add_field(name='Game :video_game:',value='stacy help game',inline=True)
        embed.add_field(name='Image :frame_photo:',value='stacy help image',inline=True)
        embed.add_field(name='Nerd stuff :nerd:',value='stacy help nerd',inline=True)
    elif str(cat).capitalize() == "Misc":
        embed = Embed(title="List of commands in Misc",description="Displaying list of all commands in misc",color=color())
        embed.add_field(name='69',value='Sends Nice! :smiling_imp: ', inline=True)
        embed.add_field(name='luv',value='Stacy:',inline=True)
        embed.add_field(name='your_age', value='Stacy sends her age :one: :seven:',inline=True)
        embed.add_field(name='am i right',value='stacy backs you up on an argument :100: ',inline=True)
        embed.add_field(name='hi',value="stacy say's hi in 5 diffrent langs :wave: ",inline=True)
    elif str(cat).capitalize() == "Fun":
        embed = Embed(title="List of commands in fun",description="Displaying list of all commands in fun",color=color())
        embed.add_field(name='burn',value='stacy insults a person :fire:\n Hey nothing personal!:rofl:', inline=True)
        embed.add_field(name='spam',value='stacy spam 5 @mention_user ',inline=True)
        embed.add_field(name='meme', value='stacy sends a funny meme :rofl:',inline=True)
        embed.add_field(name='mgen',value='stacy mgen 5 test, test ',inline=True)
        embed.add_field(name='z',value='stacy z weak ',inline=True)
        embed.add_field(name='compliment',value='stacy compliment @mention_user',inline=True)
        embed.add_field(name='yodish',value='stacy yodish I like the way you smile :green_circle:',inline=True)
    elif str(cat).capitalize() == "Music":
        embed = Embed(title="List of commands in music",description="Displaying list of all commands in music",color=color())
        embed.add_field(name='play',value='Stacy play putin bad history \n', inline=True)
        embed.add_field(name='pause',value='Pauses a playing music :pause_button: ',inline=True)
        embed.add_field(name='resume', value='Resumes a paused music :play_pause:',inline=True)
        embed.add_field(name='stop',value='Stops all songs and clears queue :octagonal_sign: ',inline=True)
        embed.add_field(name='server-play',value='plays server playlist :file_cabinet: *is being refactored',inline=True)
    elif str(cat).capitalize() == "Game":
        embed = Embed(title="List of commands in game",description="Displaying list of all commands in game",color=color())
        embed.add_field(name='RPS',value='Plays Rock:rock: Paper:page_facing_up: Scissors:scissors: ',inline=True)
        embed.add_field(name='rr',value="Play Russian Roulette :gun:")
        embed.add_field(name='xo',value="Play Tic-Tac-Toe")
        embed.add_field(name='Akinator',value="Play Akinator")
    elif str(cat).capitalize() == "Image":
        embed = Embed(title="List of commands in image",description="Displaying list of all commands in image",color=color())
        embed.add_field(name='Woof',value='Stacy woof ',inline=True)
    elif str(cat).capitalize() == "Nerd":
        embed = Embed(title="List of commands in nerd",description="Displaying list of all commands in nerd",color=color())
        embed.add_field(name='search',value='Search google for provided argument :mag:',inline=True)
        embed.add_field(name='yt',value='Search youtube for provided argument :projector:',inline=True)
        embed.add_field(name='translate',value='stacy translate thanks in japanese :earth_asia:',inline=True)
        embed.add_field(name='define',value='stacy define dork :interrobang:',inline=True)
        embed.add_field(name='dstat',value='Sends url to check Discord Status :nerd:',inline=True)
        embed.add_field(name='Suggestion',value='Suggest to add a command',inline=True)
    else:
        await ctx.send("Invalid Option")
        await hlp(ctx)
    await ctx.send(embed=embed)

@bot.command(name='?')
async def test(ctx):
    await ctx.send("Yes?")

@bot.command(name='spam')
async def spam(ctx, num=5,*,who):
    if int(num) <= 10:
        for j in range(int(num)):
            if len(who) > len("562553902990491659")+2:
                await ctx.send(f"{who}")
            else:
                await ctx.send(f"{who} you have been spammed")
    else:
        await ctx.send(f"<@!{ctx.author.id}> you can't spam more than 10 times")

@bot.command(name='hi')
async def hi(ctx):
    li = ['BONJOUR','HOLA','CIAO','HI','OLÀ']
    reply = random.choice(li)
    await ctx.send(f"{reply} <@!{ctx.author.id}>")

@bot.command(name='join')
async def join(ctx):
    global voice
    channel = ctx.author.voice.channel
    voice = get(bot.voice_clients, guild=ctx.guild)
    if voice and voice.is_connected:
        await voice.move_to(channel)
    else:
        voice = await channel.connect()

@bot.command(name='burn')
@commands.has_any_role('Burn', 'Owner','Devlopers')
async def burn(ctx,who:discord.Member):
    burn = ["You're my favorite person besides every other person I've ever met.","No offense, but you make me want to staple my cunt shut.","Did your parents have any children that lived?","I envy people who have never met you.","Maybe if you eat all that makeup you will be beautiful on the inside.","You're kinda like Rapunzel except instead of letting down your hair, you let down everyone in your life.","You're impossible to underestimate.","You're not the dumbest person on the planet, but you sure better hope he doesn't die.","I'll plant a mango tree in your mother's cunt and fuck your sister in its shade.","Those aren't acne scars, those are marks from the coat hanger.","You have more dick in your personality than you do in your pants.","If you were an inanimate object, you'd be a participation trophy.","You look like your father would be disappointed in you if he stayed.","You're not pretty enough to be that dumb.","Your mother fucks for bricks so she can build your sister a whorehouse.","I'm sorry your dad beat you instead of cancer.","You were birthed out your mother's ass because her cunt was too busy.","You're so stupid you couldn't pour piss out of a boot if the directions were written on the heel.","Take my lowest priority and put yourself beneath it.","Such a shame your mother didn't swallow you.","The best part of you ran down your mom's leg.","You couldn't organize a blowjob if you were in a Nevada brothel with a pocket full of hundred-dollar bills.","You are a pizza burn on the roof of the world's mouth.","Does your ass ever get jealous of the shit that comes out of your mouth?","People like you are the reason God doesn't talk to us anymore.","You're so dense, light bends around you.","I'd love to stay and chat but I'd rather have type-2 diabetes.","You should put a condom on your head, because if you're going to act like a dick you better dress like one, too.","I bet you swim with a T-shirt on.","How the fuck are you the sperm that won?","May your balls turn square and fester at the corners.","I hope your wife brings a date to your funeral.","If you were a potato you'd be a stupid potato.","Your face looks like it was set on fire and put out with chains.","You might want to get a colonoscopy for all that butthurt.","Your mother may have told you that you could be anything you wanted, but a douchebag wasn't what she meant.","You are so ugly that when you were born, the doctor slapped your mother.","You look like two pounds of shit in a one-pound bag.","Ready to fail like your dad's condom?","I'd call you a cunt, but you have neither the warmth or the depth.","If I wanted to commit suicide I'd climb to your ego and jump to your IQ.","You make me wish I had more middle fingers.","If genius skips a generation, your children will be brilliant.","Everyone that has ever said they love you was wrong.","You have the charm and charisma of a burning orphanage.","If there was a single intelligent thought in your head it would have died from loneliness.","I don't have the time or the crayons to explain this to you.","The only difference between you and Hitler is Hitler knew when to kill himself.","You're dumber than I tell people.","I hope you have beautiful children and that they all get cancer.","Your birth certificate is an apology letter from the condom factory.","For years your mother and I wanted kids. Imagine our disappointment when you came along.","Your face is so oily that I'm surprised America hasn't invaded yet.","Your father should've wiped you on the sheets.","If I wanted any shit out of you I'd take my dick out of your ass.","I can explain it to you, but I can't understand it for you.","You're the reason you mom swallows now.","How did you crawl out of the abortion bucket?","If the road were paved with dicks, your mother would walk on her ass.","You are the stone in the shoes of humanity.","You could fuck up a wet dream.","You're not as dumb as you look.","Son, anyone who would fuck you ain't worth fucking.","This is why everyone talks about you as soon as you leave the room.","The smartest thing that ever came out of your mouth was my dick.","You know, people were right about you.","You've got a great body. Too bad there's no workout routine for a face.","If you could suck your own dick then you would finally suck at everything.","I want you to be the pallbearer at my funeral so you can let me down one last time.","Don't make me have to smack the extra chromosome out of you.","You've gotta be two people, because no single person can be that stupid.","If you were any dumber, someone would have to water you twice a week.","You'll never be half the man your mother was.","If you were on fire and I had a cup of my own piss, I'd drink it.","I've forgotten more than you know.","You smell like you wipe back to front.","I could agree with you, but then we'd both be wrong.","Shut your mouth, I can smell your Dad's cock.","You look like something I drew with my left hand.","How do you even masturbate knowing whose dick you're touching?","You are the human embodiment of an eight-dollar haircut.","The only thing that will ever fuck you is life.","You suck dick at fucking pussy.","If you were twice as smart, you'd still be stupid. I can't forget that day.","You look like a bag of mashed-up assholes.","You're a huge bag of tiny cocks.","You're so inbred you're a sandwich.","In a country where anyone can be anything, I will never understand why you chose to be mediocre.","What’s a girl like you doing at a nice place like this?","You're about as important as a white crayon.","Was your mother just in the bathroom? Because she forgot to flush your twin.","If my dog had a face like you, I'd paint his ass and teach him to walk backwards.","If your parents were to divorce, would they still be brother and sister?","You look like the kind of person that buys condoms on his way to a family reunion.","People will not only remember your death, they will celebrate it.","You are a shit stain on the underpants of society.","You look like you were poured into your clothes but someone forgot to say when to stop.","You're about as useful as tits on a pigeon.","Why are you playing hard to get when you're so hard to want?","I'd offer you a shit sandwich, but I hear you don't like bread."]
    reply = random.choice(burn)
    jin = ["I'll plant a mango tree in your mother's cunt and fuck your sister in its shade.","Your mother fucks for bricks so she can build your sister a whorehouse.","You look like the kind of person that buys condoms on his way to a family reunion."]
    jinr = random.choice(jin)
    if medit(who.edit).get('Member id') == 562553902990491659:
        if ctx.author.id == 562553902990491659:
            await ctx.send("Sorry but I can't burn you Abhay")
        else:
            await ctx.send("Sorry but I can't burn Abhay")
            await ctx.send("Here is a Message from Abhay")
            await ctx.send(f"<@!{ctx.author.id}> {reply}")
    elif medit(who.edit).get('Member id') == 772381863528103966 or medit(who.edit).get('Member id') == 834409783502438480:
        if ctx.author.id == 562553902990491659:
            await ctx.send("I Know it was just for testing")
        elif medit(who.edit).get('Member id') == 834409783502438480:
            await ctx.send(f"Burn steve...?")
            await ctx.send(f"Nah Instead...")
            await ctx.send(f"<@!{ctx.author.id}> {jinr}")
        else:
            await ctx.send(f"Hey listen you cozy motherfucker, <@!{ctx.author.id}> {jinr}")
    else:
        await ctx.send(f"{who.mention} {reply}")

@burn.error
async def burn_error(ctx,error):
    if isinstance(error, commands.errors.MissingRole):
        embed = ebed("Missing Role")
        embed.add_field(name='Needed Role',value="Burn")
        await ctx.send(embed=embed)

@bot.command(name='69')
async def _69(ctx):
    await ctx.send("Nice!")

@bot.command(name='say_sorry_to')
async def say_sorry(ctx, who:discord.Member):
    def check(m):
        return m.author.id == ctx.author.id

    if ctx.author.id == 562553902990491659:
        await ctx.send('Abhay, but...')
        try:
            say = await bot.wait_for('message', check=check, timeout=10)
        except asyncio.TimeoutError:
            await ctx.send(f"Aight {who.mention} I'm Sorry")
        if str(say).capitalize() == "Say":
            await ctx.send(f"Okay, hey {who.mention} I'm Sorry")
        else:
            await ctx.send(f"Okay, hey {who.mention} I'm Sorry")
    else:
        await ctx.send(f"Fuck Off")

@bot.command(name='knock')
async def knock_knock(ctx):
    jokes = ["Knock knock. \nWho's there?\n Hawaii. \nHawaii who? \n I'm fine, Hawaii you?","Knock knock. \nWho's there?\n Voodoof \nVoodoo who? \n Voodoo you think you are, asking me so many questions?","Knock knock. \nWho's there?\n Nana. \nNana who? \n Nana your business.","Knock knock. \nWho's there?\n Hatch. \nHatch who? \n God bless you.","Knock knock. \nWho's there?\n Mustache. \nMustache who? \n Mustache you a question, but I'll shave it for later","Knock knock. \nWho's there?\n Amish. \n Amish who? \n Really? You don't look like a shoe.","Knock knock. \nWho's there?\n Gorilla. \nGorilla who? \n Gorilla me a hamburger.","Knock knock. \nWho's there?\n Tank. \nTank who? \n You're welcome.","Knock knock. \nWho's there?\n Turnip. \nTurnip who? \n Turnip the volume, I love this song!","Knock knock. \nWho's there?\n Adore. \nAdore who? \n Adore is between us. \nOpen up!","Knock knock. \nWho's there?\n Daisy!  Daisy who? \n Daisy me rollin, they hatin'.","Knock knock. \nWho's there?\n Aida. \nAida who? \n Aida sandwich for lunch today.","Knock knock. \nWho's there?\n Cargo. \nCargo who? \n No, car go 'beep beep'!","Knock knock. \nWho's there?\n Icing. \nIcing who? \n Icing so loud, the neighbors can hear.","Knock knock. \nWho's there?\n Alpaca. \nAlpaca who? \n Alpaca the trunk, you pack the suitcase.","Knock knock. \nWho's there?\n Cereal. \nCereal who? \n Cereal pleasure to meet you!","Knock knock. \nWho's there?\n A little old lady. \nA little old lady who? \n Dang! All this time, I had no idea you could yodel.","Knock knock. \nWho's there?\n Keith! Keith who? \n Keith me, my thweet preenth!","Knock knock. \nWho's there?\n Lettuce. \nLettuce who? \n Lettuce in it's cold out here.","Knock knock. \nWho's there?\n Oswald. \nOswald who? \n Oswald my bubble gum!","Knock knock. \nWho's there?\n Boo. \nBoo who? \n Hey, don't cry!","Knock knock. \nWho's there?\n An extraterrestrial. \nAn extraterrestrial who? \n Wait, how many extraterrestrials do you know?","Knock knock. \nWho's there?\n Police. \nPolice who? \n Police stop telling these awful knock knock jokes!","Knock knock. \nWho's there?\n Candice. \nCandice who? \n Candice door open or what?","Knock knock. \nWho's there?\n Control Freak. \nCon— Okay, now you say, \"Control Freak who? \n!\"","Knock knock. \nWho's there?\n Theodore! Theodore who? \n Theodore wasn't open so I knocked.","Knock knock. \nWho's there?\n Yah. \nYah who? \n No thanks, I use Bing or Google.","Knock knock. \nWho's there?\n Snow. \nSnow who? \n Snow use. \nI forgot my name again!","Knock knock. \nWho's there?\n Robin. \nRobin who? \n Robin you, now hand over the cash!","Knock knock. \nWho's there?\n Pecan!  Pecan who? \n Pecan somebody your own size!","Knock knock. \nWho's there?\n Dwayne. \nDwayne who? \n Dwayne the bathtub already. \nI'm drowning!","Knock knock. \nWho's there?\n Annie. \nAnnie who? \n Annie way you can let me in now?","Knock knock. \nWho's there?\n Cantaloupe!  Cantaloupe who? \n Cantaloupe to Vegas, our parents would get mad.","Knock knock. \nWho's there?\n Spell. \nSpell who? \n Okay, fine. \nW-H-O.","Knock knock. \nWho's there?\n Water. \nWater who? \n Water those plants or they're going to die!","Knock knock. \nWho's there?\n Euripides. \nEuripides who? \n Euripides jeans, you pay for 'em.","Knock knock. \nWho's there?\n Closure. \nClosure who? \n Closure mouth while you're chewing!","Knock knock. \nWho's there?\n Cash. \nCash who? \n No thanks, but I'd love some peanuts!","Knock knock. \nWho's there?\n Olive. \nOlive who? \n Olive you and I don't care who knows it!","Knock knock. \nWho's there?\n Tyrone. \nTyrone who? \n Tyrone shoelaces!","Knock knock. \nWho's there?\n Figs! Figs who? \n Figs the doorbell, it's broken!","Knock knock. \nWho's there?\n Ho-ho. \nHo-ho who? \n You know, your Santa impression could use a little work.","Knock knock. \nWho's there?\n Yacht. \nYacht who? \n Yacht to know me by now!","Knock knock. \nWho's there?\n Dishes. \nDishes who? \n Dishes a nice place you got here.","Knock knock. \nWho's there?\n Mikey! Mikey who? \n Mikey doesn't fit in the keyhole!","Knock knock. \nWho's there?\n Owls say. \nOwls say who? \n Yes, they do.","Knock knock. \nWho's there?\n Déja. \nDéja who? \n Knock knock!","Knock knock. \nWho's there?\n Mike Snifferpippets. \nMike Snifferpippets who? \n Oh come on, how many Mike Snifferpippets do you know?","Knock knock. \nWho's there?\n Sherlock. \nSherlock who? \n Sherlock your door tight.","Knock knock. \nWho's there?\n Rhino!  Rhino who? \n Rhino every knock knock joke there is!","Knock knock. \nWho's there?\n Goliath. \nGoliath who? \n Goliath down, thou looketh tired!","Knock knock. \nWho's there?\n Leena. \nLeena who? \n Leena little close and I will tell you!","Knock knock. \nWho's there?\n Juno. \nJuno who? \n Juno I love you, right?","Knock knock. \nWho's there?\n Witches. \nWitches who? \n Witches the way to the store.","Knock knock. \nWho's there?\n Beets! Beets who? \n Beets me!","Knock knock. \nWho's there?\n To. \nTo who? \n It's to whom.","Knock knock. \nWho's there?\n Avenue. \nAvenue who? \n Avenue seen it coming?","Knock knock. \nWho's there?\n Broken pencil. \nBroken pencil who? \n Never mind, there's no point!","Knock knock. \nWho's there?\n Ice Cream Soda. \nIce Cream Soda who? \n Ice Cream Soda whole neighborhood can hear!","Knock knock. \nWho's there?\n Egg. \nEgg who? \n Eggstremely disappointed you still don't recognize me.","Knock knock. \nWho's there?\n Zany. \nZany who? \n Zany body home?","Knock knock. \nWho's there?\n Teresa. \nTeresa who? \n Teresa are green!","Knock knock. \nWho's there?\n Iran. \nIran who? \n Iran over here to tell you this!","Knock knock. \nWho's there?\n Dozen. \nDozen who? \n Dozen anybody want to let me in?!","Knock knock. \nWho's there?\n Amanda. \nAmanda who? \n Amanda fix your sink!","Knock knock. \nWho's there?\n Nun. \nNun who? \n Nun your business!","Knock knock. \nWho's there?\n Jess. \nJess who? \n Jess cut the talking and open the door!","Knock knock. \nWho's there?\n Me. \nMe who? \n Having an identity crisis, are you?","Knock knock. \nWho's there?\n Hatch. \nHatch who? \n Bless you!","Knock knock. \nWho's there?\n Zoom. \nZoom who? \n Zoom did you expect!","Knock knock. \nWho's there?\n FBI. \nFB… We're asking the questions here.","Knock knock. \nWho's there?\n Mikey. \nMikey who? \n Mikey got lost; open up!","Knock knock. \nWho's there?\n Ben. \nBen who? \n Ben hoping I can come in!","Knock knock. \nWho's there?\n Cook. \nCook who? \n Yeah, you do sound crazy!","Knock knock. \nWho's there?\n Noise. \nNoise who? \n Noise to see you!","Knock knock. \nWho's there?\n Noah. \nNoah who? \n Noah good place we can go get lunch?","Knock knock. \nWho's there?\n Leaf. \nLeaf who? \n Leaf me alone!","Knock knock. \nWho's there?\n Aaron. \nAaron who? \n Why Aaron you opening the door?","Knock knock. \nWho's there?\n Orange. \nOrange who? \n Orange you even going to open the door!","Knock knock. \nWho's there?\n Woo. \nWoo who? \n Don't get so excited, it's just a joke.","Knock knock. \nWho's there?\n Yukon. \nYukon who? \n Yukon say that again!","Knock knock. \nWho's there?\n Amarillo. \nAmarillo who? \n Amarillo nice guy.","Knock knock. \nWho's there?\n Andrew! Andrew who? \n Andrew a picture!","Knock knock. \nWho's there?\n Armageddon. \nArmageddon who? \n Armageddon a little bored. \nLet's go out.","Knock knock. \nWho's there?\n Bruce. \nBruce who? \n I Bruce easily, don't hit me !","Knock knock. \nWho's there?\n Butter. \nButter who? \n Butter be quick, I have to go to the bathroom!","Knock knock. \nWho's there?\n Bed. \nBed who? \n Bed you can't guess who I am!","Knock knock. \nWho's there?\n CD. \nCD who? \n CD guy on your doorstep?","Knock knock. \nWho's there?\n Cows go. \nCow's go who? \n No, silly. \nCows go \"moo!\"","Knock knock. \nWho's there ? Dishes! Dishes who? \n Dishes the Police come out with your hands up.","Knock knock. \nWho's there?\n Interrupting doctor. \nInter– You've got cancer.","Knock knock. \nWho's there?\n Ears. \nEars who? \n Ears another knock knock jokes for you!","Knock knock. \nWho's there?\n Ferdie! Ferdie who? \n Ferdie last time open this door!","Knock knock. \nWho's there?\n Iona. \nIona who? \n Iona new car!","Knock knock. \nWho's there?\n Ivor. \nIvor who? \n Ivor you let me in or I'll climb through the window.","Knock knock. \nWho's there?\n Keanu. \nKeanu who? \n Keanu let me in, it's cold out here !","Knock knock. \nWho's there?\n Kanga. \nKanga who? \n Actually, it's kangaroo!","Knock knock. \nWho's there?\n Luke. \nLuke who? \n Luke through the the peep hole and find out.","Knock knock. \nWho's there?\n A little boy. \nA little boy who? \n A little boy who can't reach the doorbell.","Knock knock. \nWho's there?\n Lion. \nLion who? \n Lion on your doorstep, open up!"]
    reply = random.choice(jokes)
    await ctx.send(f"{reply}")

@bot.command(name='leavevc', aliases=['leave'])
async def leave(ctx):
    await ctx.voice_client.disconnect()

@bot.command(name='thanks')
async def your_wel(ctx):
    await ctx.send(f"No problem, ")

@bot.command(name='meme')
async def meme(ctx):
    link = await fetch()
    url = link[0]
    name = link[1]
    embed = ebed(name)
    embed.set_image(url=url)
    embed.set_footer(text=f"Sauce {link[2]}")
    await ctx.send(embed=embed)



@bot.command(name='your_age')
async def age(ctx):
    await ctx.send(f"I'm 17 <@!{ctx.author.id}>")

@bot.command(name='right?', aliases=['am','right','correct?'])
async def opi(ctx):
    ops = ['I agree with you.','Yup','We are of one mind.','You can say that again.','I could not agree with you more.','That’s right.','Agreed.','You took the words right out of my mouth.','We’re in accord.','I agree.','Yeah','Yep','Absolutely!','You’re absolutely right.','Exactly!','You’ve found.','You are so right.','You got it.','I could not have said it any better.','Our thoughts are parallel.','My thoughts exactly.','Affirmative.','No doubt about it.','I guess so.','I had that same idea.','That’s exactly how I feel.','I’d go along with that.','That’s just what I was thinking.','Of course.','Thats exactly what I was thinking.','Sure.','I think you are totally right about that.','I don’t doubt you’re right.','I see what you mean…','You’ve hit the nail on the head.','So do I.','I fee that way too.','Me too.','Definitely.']
    reply = random.choice(ops)
    await ctx.send(f'{reply}')

@bot.command(name='compliment')
async def comp(ctx, who):
    comps = ['Your positivity is infectious. ','You should be so proud of yourself.','You’re amazing!','You’re a true gift to the people in your life.','You’re an incredible friend.','I really appreciate everything that you do.','You inspire me to be a better person.','Your passion always motivates me.','Your smile makes me smile.','Thank you for being such a great person.','The way you carry yourself is truly admirable.','You are such a good listener.','You have a remarkable sense of humor.','Thanks for being you!','You set a great example for everyone around you.','I love your perspective on life.','Being around you makes everything better.','You always know the right thing to say.','The world would be a better place if more people were like you!','You are one of a kind.','You make me want to be the best version of myself.','You always have the best ideas.','I’m so lucky to have you in my life.','Your capacity for generosity knows no bounds.','I wish I were more like you.','You are so strong.','I’ve never met someone as kind as you are.','You have such a great heart.','Simply knowing you has made me a better person.','You are beautiful inside and out.','You are so special to everyone you know.','Your mere presence is reassuring to me.','Your heart must be 10 times the average size.','You are my favorite person to talk to.','You’ve accomplished so many incredible things.','I appreciate your friendship more than you can know.','I love how you never compromise on being yourself.','I tell other friends how wonderful you are.','You helped me realize my own worth.','Your point of view is so refreshing.','You always make me feel welcome.','You deserve everything you’ve achieved.','I am so proud of your progress.','I’m lucky just to know you.','You are so down to earth.','You know just how to make my day!','You spark so much joy in my life.','Your potential is limitless.','You have a good head on your shoulders.','You are making a real difference in the world.','You’re so unique.','You are wise beyond your years.','You’re worthy of all the good things that come to you.','Your parents must be so proud.','How did you learn to be so great? ','Never stop being you!','No one makes me laugh harder than you do.','You inspire me in so many different ways.','You continue to impress me.','You make the small things count.','You’re a constant reminder that people can be good.','I admire the way that you challenge me.','You make me see things in an entirely new way.','Thanks for always being there for me.','You are a ray of sunshine.','You have the courage of your convictions.','On a scale of one to ten, you’re an eleven.','You’re incredibly thoughtful.','You have the best ideas.','You’re the most perfect ‘you’ there is.','You are the epitome of a good person.','You always know how to find the silver lining.','You’re the person that everyone wants on their team.','I always learn so much when I’m around you.','Is there anything you can’t do!?']
    reply = random.choice(comps)
    if who == 'me' or who == 'ME' or who == 'Me':
        await ctx.send(f"<@!{ctx.author.id}> {reply}")
    else:
        await ctx.send(f"{who} {reply}")

@bot.command(name='define')
async def urbandict(ctx, *what):
    word = ''
    for x in what:
        word += f'{x} '
    word = word.rstrip()
    await ctx.send(f"I'm curious too to know what {word} is")
    reply = udict(word)
    if reply != "This word has no meaning whatsoever... Please check your word and try again.":
        embed = Embed(title=f"{word}",color=color())
        embed.add_field(name="Meaning",value=f"{reply}",inline=True)
        await ctx.send(embed=embed)
    else:
        embed = Embed(title=f"Check your word",color=color())
        embed.add_field(name="Troubleshoot",value=f"{reply}",inline=True)
        await ctx.send(embed=embed)

@bot.command(name='woof')
async def woof(ctx, *, breed=None):
    if breed is None:
        URl = 'https://dog.ceo/api/breeds/image/random'
        respnse = requests.request('GET',url=URl).json()
        url = respnse.get('message')
    else:
        URl = f"https://dog.ceo/api/breed/{breed}/images/random"
        respnse = requests.request('GET',url=URl).json()
        url = respnse.get('message')

    embed = Embed(title='Woof :dog:',description="Aren't they lovely",color=0xde3069)
    embed.set_image(url=url)
    embed.set_footer(text="listings of more cute breeds are husky, pitbull, dalmatian, doberman. etc")
    await ctx.send(embed=embed)

@bot.command(name='z')
async def meme_templates(ctx,which='help',*,who:discord.Member=None):
    author_nick = None
    nick = None
    if who != None:
        nick = who.nick
        if nick is None:
            nick = who
    author_nick = ctx.author.nick
    if author_nick is None:
        author_nick = ctx.author
    
    

    dt = {
        'bird':{'url':'https://preview.redd.it/nm93snupy1i31.jpg?width=960&crop=smart&auto=webp&s=1132abe7a6cc0bcb804cd3c12b1f3aca1a7d6f6f',
        'name':'Listen here you lil sh*t'},
        'day':{'url':'https://preview.redd.it/7rtkq25zvj751.jpg?width=960&crop=smart&auto=webp&s=c97b88cd644da420598d2e83e462e3451f690609',
        'name':'Understandable have a great day'},
        'ded':{'url':'https://thumbs.gfycat.com/ConsciousSardonicCaribou-max-1mb.gif',
        'name':"HAHA mannn I'm dead"},
        'brain':{'url':'https://newfastuff.com/wp-content/uploads/2019/06/77movef.png',
        'name':'Yea... this is big brain time'},
        'our':{'url':'https://en.meming.world/images/en/thumb/a/a9/Communist_Bugs_Bunny.jpg/300px-Communist_Bugs_Bunny.jpg',
        'name':'Our.. :raised_hands: :raised_hands: '},
        'tom':{'url':'https://nyc3.digitaloceanspaces.com/memecreator-cdn/media/__processed__/263/template-unsettled-tom-0c6db91aec9c.jpg',
        'name':'Huh! Wat?'},
        'tom2':{'url':'https://i.imgur.com/TbtFx6g.jpg',
        'name':"I'm speech less"},
        'tom3':{'url':'https://newfastuff.com/wp-content/uploads/2019/03/meme_template_73.png',
        'name':'Self burn.. those are rare'},
        'tom4':{'url':'https://cdn131.picsart.com/301644734236201.jpg?type=webp&to=min&r=640',
        'name':'Think bout her...'},
        'jerry':{'url':'https://i.imgflip.com/1ll661.jpg',
        'name':'*weird screaming noises'},
        'abey':{'url':'https://memeadda.com/uploads/memes/abey-saale-moin-akhtar-1595528771.webp',
        'name':'Abey Saaley'},
        'ban':{'url':'https://i.pinimg.com/736x/81/da/f8/81daf8311f01426b311249df11f36a33.jpg',
        'name':'Nice meme your reward is...'},
        'why':{'url':'https://i.redd.it/vwdzr31u7kx51.png',
        'name':"why cause fuck'em that's why"},
        'shame':{'url':'https://i.kym-cdn.com/entries/icons/original/000/030/021/Screen_Shot_2019-06-10_at_2.40.34_PM.jpg',
        'name':'Press Y to Shame'},
        'fan':{'url':'https://i.kym-cdn.com/entries/icons/original/000/034/494/Screen_Shot_2020-06-30_at_11.31.33_AM.png',
        'name':"*Satan: Well I just wanna say that I'm a huge fan"},
        'weak':{'url':'https://i.pinimg.com/originals/a5/e1/f2/a5e1f283edc34873385d2bed6b2f144e.jpg',
        'name':'Weakness disgusts me'},
        'crow':{'url':'https://i.imgflip.com/37qtje.jpg',
        'name':'Crow Of Judgement'},
        'nahi':{'url':'https://i.imgur.com/zLkmP0A.gif',
        'name':'Nahi.! XD'},
        'god':{'url':'https://i.pinimg.com/originals/31/51/8a/31518ab4f03b12f85ee1ef71eaf99a9b.gif',
        'name':'Huh Puny God!'},
        'limits':{'url':'https://media1.tenor.com/images/c49a4d3f4b907600b989500d0944ba96/tenor.gif?itemid=18198051',
        'name':'Please I request you'},
        'limit':{'url':'https://media1.tenor.com/images/c49a4d3f4b907600b989500d0944ba96/tenor.gif?itemid=18198051',
        'name':'Please I request you'},
        'mariyada':{'url':'https://media1.tenor.com/images/c49a4d3f4b907600b989500d0944ba96/tenor.gif?itemid=18198051',
        'name':'Please I request you'},
        'liar':{'url':'https://i.imgflip.com/3j0rr1.png',
        'name':'Liar!'},
        'tables':{'url':'https://gph.is/g/am6kkNX',
        'name':'Well well well how the turntables'},
        'stfu':{'url':'https://media.giphy.com/media/11StaZ9Lj74oCY/giphy.gif',
        'name':f'*{author_nick} clapped {nick}'},
        'lol_no':{'url':'https://cdn.discordapp.com/attachments/690818384321052714/829208012827787274/image0.png',
        'name':'hahaha.... -_- No'}
        }
    if which == 'help':
        li = ''
        for x in dt:
            li += f'{x} _|_ '
        embed = Embed(title='Meme template help',description='List of all meme templates',color=color())
        embed.add_field(name='Use stacy z _any_template_below_', value=li , inline=False)
        await ctx.send(embed=embed)
        return
        

    tmp = which
    meme = dt.get(tmp)
    embed = ebed(meme.get('name'))
    embed.set_image(url=meme.get('url'))
    await ctx.send(embed=embed)

@bot.command(name='mgen')
async def meme_genratre(ctx, index: str, *t1):
    data = {
        '1':'181913649',
        '2':'247375501',
        '3':'259237855',
        '4':'217743513',
        '5':'438680',
        }
    i = data.get(index)

    li = []
    str1 = ""
    for g in t1:
        li.append(g)
    
    for k in li:
        str1 += f'{k} '

    replace1 = str1.replace(' ','.')
    replace2 = replace1.replace(',',' ')
    split = replace2.split()
    replace3 = split[0].replace('.', ' ')
    replace4 = split[1].replace('.', ' ')

    text1 = replace3
    text2 = replace4


    url = memegen(i,text1,text2)
    embed = ebed("Haha.! Nice One.! :joy: :joy: :joy: ")
    embed.set_image(url=url)
    await ctx.send(embed=embed)

@bot.command(name='search')
async def srchh(ctx, *what):
    string = ""
    for i in what:
        string += f'{i} '
    string.rstrip()
    ans = srch(string)
    await ctx.send(ans)

@bot.command(name='http')
async def http(ctx, code:int):
    await ctx.send(f'https://http.cat/{code}')

@bot.command(name='translate')
async def tlate(ctx, *text):

    txt = ""
    q = 0
    for x in text:
        q += 1

    for z in range(0,q-2):
        txt += f'{text[z]} '

    language = text[-1]
    language = str(language).capitalize()
    langs = {'Afrikaans':'af','Albanian':'sq','Amharic':'am','Arabic':'ar','Armenian':'hy','Azerbaijani':'az','Basque':'eu','Belarusian':'be','Bengali':'bn','Bosnian':'bs','Bulgarian':'bg','Catalan':'ca','Cebuano':'ceb','Chinese':'zh-CN','Chinese2':'zh-TW','Corsican':'co','Croatian':'hr','Czech':'cs','Danish':'da','Dutch':'nl','English':'en','Esperanto':'eo','Estonian':'et','Finnish':'fi','French':'fr','Frisian':'fy','Galician':'gl','Georgian':'ka','German':'de','Greek':'el','Gujarati':'gu','Haitian Creole':'ht','Hausa':'ha','Hawaiian':'haw','Hebrew':'he','Hindi':'hi','Hmong':'hmn','Hungarian':'hu','Icelandic':'is','Igbo':'ig','Indonesian':'id','Irish':'ga','Italian':'it','Japanese':'ja','Javanese':'jv','Kannada':'kn','Kazakh':'kk','Khmer':'km','Kinyarwanda':'rw','Korean':'ko','Kurdish':'ku','Kyrgyz':'ky','Lao':'lo','Latin':'la','Latvian':'lv','Lithuanian':'lt','Luxembourgish':'lb','Macedonian':'mk','Malagasy':'mg','Malay':'ms','Malayalam':'ml','Maltese':'mt','Maori':'mi','Marathi':'mr','Mongolian':'mn','Myanmar (Burmese)':'my','Nepali':'ne','Norwegian':'no','Nyanja':'ny','Odia':'or','Pashto':'ps','Persian':'fa','Polish':'pl','Portuguese':'pt','Punjabi':'pa','Romanian':'ro','Russian':'ru','Samoan':'sm','Scots Gaelic':'gd','Serbian':'sr','Sesotho':'st','Shona':'sn','Sindhi':'sd','Sinhala (Sinhalese)':'si','Slovak':'sk','Slovenian':'sl','Somali':'so','Spanish':'es','Sundanese':'su','Swahili':'sw','Swedish':'sv','Tagalog (Filipino)':'tl','Tajik':'tg','Tamil':'ta','Tatar':'tt','Telugu':'te','Thai':'th','Turkish':'tr','Turkmen':'tk','Ukrainian':'uk','Urdu':'ur','Uyghur':'ug','Uzbek':'uz','Vietnamese':'vi','Welsh':'cy','Xhosa':'xh','Yiddish':'yi','Yoruba':'yo','Zulu':'zu'}

    tint = langs.get(f'{language}')
    if tint is None:
        await ctx.send("The Language of Aliens I don't know that")
        return
    tlated = translator.translate(f'{txt}', dest=f'{tint}')

    def get_key(val):
        for key, value in langs.items():
             if val == value:
                 return key
        return None

    
    source = get_key(tlated.src)
    dst = get_key(tlated.dest)    

    
    embed = Embed(title=f'Tanslation for {txt} in {language}',color=color())
    embed.add_field(name="Translated text: ",value=f'{tlated.text}',inline=False)
    embed.add_field(name="Pronunciation: ",value=f'{tlated.pronunciation}',inline=False)
    embed.add_field(name="Source language: ",value=f'{source}',inline=True)
    embed.add_field(name="Translated in: ",value=f'{dst}',inline=True)
    await ctx.send(embed = embed)

@bot.command(name='countdown')
async def countdown(ctx, secs:int):
    if secs <= 10:
        await ctx.send(f"Countdown initiated for {secs}")
        for x in range(0,secs):
            await ctx.send(x)
            await asyncio.sleep(1)
    elif secs <= 60 and secs >=11:
        cal = (secs/100) * 10
        await ctx.send(f"Countdown initiated for {secs}")
        t = 0
        for x in range(0,11):
            await ctx.send(t)
            t = t + int(cal)
            await asyncio.sleep(cal)
    else:
        await ctx.send("Countdown more than 1 minute!!! You nuts?!! What am I your fucking clock?!!")
        
@bot.command(name='bye')
async def bye(ctx):
    if is_owner(ctx):
        await ctx.send('Bye')
        await bot.logout()
    
@bot.command(name='rps')
async def rsp(ctx):
    def check(m):
        return m.author.id == ctx.author.id
    
    stacy = 0
    player = 0
    an = "Yes"
    await ctx.send("I was bored too yk.!")
    await ctx.send("So let's play ;)")
    while(an=="Yes"):
        x = []
        await ctx.send("Rock, Paper or Scissors")
        try:
            playerChoice = await bot.wait_for('message', check=check, timeout=30)
        except asyncio.TimeoutError:
            await ctx.send("Timed Out and By Default I won")
            an == 'No'

        a = gam(playerChoice.content)
        for c in a:
            x.append(c)

        await ctx.send(x[1])

        if x[0] == "You Win":
            await ctx.send("Aww Man I lost.!")
            await ctx.send("Let's play Another One?")
            await ctx.send("just enter 'YES/NO'")
            player += 1
            try:
                ao = await bot.wait_for('message', check=check, timeout=30)
            except asyncio.TimeoutError:
                await ctx.send("Timed Out")
                an = "No"

            if str(ao.content).capitalize() == "No":
                an = "No"

        elif x[0] == "You Lose":
            await ctx.send("Yay I Won.!")
            await ctx.send("Let's play Another One?")
            await ctx.send("just enter 'YES/NO'")
            stacy += 1
            try:
                ao = await bot.wait_for('message', check=check, timeout=30)
            except asyncio.TimeoutError:
                await ctx.send("Timed Out")
                an = "No"

            if str(ao.content).capitalize() == "No":
                an = "No"
            
        elif x[0] == "Tie":
            await ctx.send("Wow that's a tie")
            await ctx.send("Let's play Another One?")
            await ctx.send("just enter 'YES/NO'")
            
            try:
                ao = await bot.wait_for('message', check=check, timeout=30)
            except asyncio.TimeoutError:
                await ctx.send("Timed Out")
                an = "No"

            if str(ao.content).capitalize() == "No":
                an = "No"
            
            
        else:
            await ctx.send(a)

    await ctx.send(f'Final Scores Stacy:{stacy} and <@!{ctx.author.id}> : {player}')

@bot.command(name='yodish', aliases=['yoda','yodian'])
async def yoda(ctx,*,msg):
    reply = yodish(str(msg).capitalize())
    await ctx.send(reply)

@bot.command(name='kick')
async def kick(ctx, member: discord.Member,*,reason=None):
    if reason is None:
        await ctx.send(f"You must provide a reason to kick {member.mention}")
    try:
        if is_owner(ctx):
            await member.kick(reason=reason)
            await ctx.send(f"{member.mention} has been kicked")
    except:
        await ctx.send("Don't do anything you are not authorized to")

@bot.command(name='dstat')
@commands.cooldown(1,30, commands.BucketType.user)
async def discord_status(ctx):
    await ctx.send("See details on:")
    await ctx.send("https://discordstatus.com/")

@bot.command(name='bot-chat', aliases=['cb','bc','cmd'])
async def botchat(ctx, *cmd):
    await ctx.message.delete()
    reply = ''
    for x in cmd:
        reply += f'{x} '

    await ctx.send(reply)
    
@bot.command(name='warn')
async def warn(ctx,who:discord.Member,*,reason=None):
    mi = medit(who.edit)
    if reason == None:
        await ctx.send("Provide a reason")
    else:
        num = call('warnings')
        num += 1
        wn = scall(mi.get('Member id'))
        wn += 1
        embed = Embed(title="WARNING",description=f"<@!{ctx.author.id}> issued a warning for <@!{mi.get('Member id')}>",color=0xFF0000)
        embed.add_field(name=f"Case",value=f'{num}', inline=True)
        embed.add_field(name=f"Warning for {mi.get('name')}",value=f'{wn}', inline=True)
        embed.add_field(name="Reason",value=f"{reason}",inline=False)
        add(reason, mi.get('Member id'))
        await ctx.send(embed=embed)
        await who.send(f'You have been warned by {ctx.author} for\nReason: {reason}')
@warn.error
async def warn_error(ctx, error):
    if isinstance(error, commands.errors.MissingRole):
        embed = ebed("Missing Role")
        embed.add_field(name='Needed Role',value="Warning_issuer")
        await ctx.send(embed=embed)
    else:
        embed = ebed("ERROR")
        embed.add_field(name='Traceback',value=error)
        await ctx.send(embed=embed)

@bot.command(name='suggest',aliases=['suggestion','sug'])
@commands.cooldown(1,3600, commands.BucketType.user)
async def sug(ctx,*,what=None):
    if what is None:
        await ctx.send("TRY TO SUGGEST SOMETHING")
    else:
        channel = bot.get_channel(780512804930191371)
        suges(what,ctx.author.id)
        k = call('suggestions')
        embed = ebed(f'Suggestion {k}')
        embed.add_field(name='Suggestion', value=what)
        embed.set_footer(text=f'Thanks for the suggestion {ctx.author.nick}')
        await channel.send(embed=embed)
        await ctx.send('Suggestion recieved thanks')

@bot.command(name='bug')
@commands.cooldown(1,3600, commands.BucketType.user)
async def bug(ctx,*,what=None):
    if what is None:
        await ctx.send('Hey do you have any solid proof what the bug is ?')
    else:
        channel = bot.get_channel(780512762677559337)
        buggy(what,ctx.author.id)
        k = call('bugs')
        embed = ebed(f'Bug {k}')
        embed.add_field(name='Bug', value=what)
        embed.set_footer(text=f'Thanks for the reporting {ctx.author.nick}')
        await channel.send(embed=embed)
        await ctx.send('Bug reported thanks')

@bot.command(name='audio', aliases=['a','v'])
async def audio_meme(ctx,*,which=None):

    data={
        'stop':'https://www.youtube.com/watch?v=50bnHZLMqTI',
        'aggiN':'https://www.youtube.com/watch?v=DfSQfJ3SM_k',
        'why':'https://www.youtube.com/watch?v=W6oQUDFV2C0',
        'morning':'https://www.youtube.com/watch?v=E6G8Nv9f88k',
        'get help':'https://www.youtube.com/watch?v=9Deg7VrpHbM',
        'smooth':'https://www.youtube.com/watch?v=PLb720ZPcQI',
        'no':'https://www.youtube.com/watch?v=umDr0mPuyQc',
        'kira':'https://www.youtube.com/watch?v=C65oaIHsdYM',
        'bkl':'./server/memes/bkl.mp3',
        'hain ji':'./server/memes/hain.mp3',
        'gae':'https://www.youtube.com/watch?v=eEa3vDXatXg',
        'no u':'https://www.youtube.com/watch?v=DBslFRbCXnw'
    }
    if which != None:
        sound = data.get(which)
        if sound == None:
            li = []
            for x in data:
                li.append(f'{x}')
            await ctx.send(f'Please use any of the following \n ```{li}```')
            return
    else:
        li = []
        for x in data:
            li.append(f'{x}')
        await ctx.send(f'Please use any of the following \n ```{li}```')
        return
        
    await vc(ctx)        
    if sound.startswith('http'):
        FFMPEG_OPTIONS = {'before_options': '-reconnect 1 -reconnect_streamed 1 -reconnect_delay_max 5', 'options': f'-vn'}
        ydl_opts = {'format': 'bestaudio'}
        with YoutubeDL(ydl_opts) as ydl:
            info = ydl.extract_info(sound, download=False)
            URL = info['formats'][0]['url']
        if voice.is_playing():
            voice.pause()
        voice.play(discord.FFmpegOpusAudio(URL,**FFMPEG_OPTIONS),after=lambda e: asyncio.run_coroutine_threadsafe(voice.disconnect(), bot.loop))
    else:
        voice.play(discord.FFmpegOpusAudio(sound),after=lambda e: asyncio.run_coroutine_threadsafe(voice.disconnect(), bot.loop))

    
    

@bot.event
async def on_command_error(ctx,error):
    if isinstance(error,commands.errors.CommandNotFound):
        embed = ebed("I don't have that command")
        embed.add_field(name='Suggestion',value='To add this command \nUse stacy sug [Feature you want]')
        await ctx.send(embed=embed)
    elif isinstance(error, commands.errors.MemberNotFound):
        embed = ebed("Member with that name was not found")
        embed.add_field(name='Suggestion',value='Mention a user to use that command')
        await ctx.send(embed=embed)
    elif isinstance(error, commands.errors.CommandOnCooldown):
        embed = Embed(title='Cooldown',description=f'You need to wait {error.retry_after} to use that command again',color=color())
        await ctx.send(embed=embed)
    elif isinstance(error, commands.errors.MissingRole):
        embed = ebed("Missing Role")
        embed.add_field(name='Needed Role',value="Owner")
        embed.add_field(name='Suggestion',value="Don't do something you are not authorized to...",inline=False)
        await ctx.send(embed=embed)
    else:
        chan = bot.get_channel(836526751794200587)
        embed = ebed('Error')
        embed.add_field(name='Error',value=error)
        embed.add_field(name='Suggestion', value="Please report the bug if you are facing the problem continuously, using \n stacy bug [The bug you are facing]")
        await chan.send(embed=embed)

@bot.event
async def on_member_join(member):
    
    channel = bot.get_channel(member.guild.system_channel.id)
    gid = member.guild.id
    if gid == 765552708290609162:
        channel = bot.get_channel(765651325307322368)
    elif gid == 395550925340540930:
        channel = bot.get_channel(570853360232824833)

    try:
        url = f'https://oughtenf.sirv.com/Discord/discordwelcome.jpg?text.0.text=Welcome%20to%20the%20crew%20%0A{member.name}&text.0.position.gravity=north&text.0.position.y=2%25&text.0.size=58&text.0.color=ffffff&text.0.font.family=Amatic%20SC&text.0.font.weight=700&text.0.background.color=ffffff&text.0.outline.width=3'
        embed = Embed(title=f'Welcome to {member.guild} :wink:')
        embed.set_image(url=url)
        embed.set_footer(text="Enjoy your stay here")
        await channel.send(embed=embed)
    except:
        pass

@bot.event
async def on_member_remove(member):
    gid = member.guild.id
    if gid == 829743733577744416:
        channel = bot.get_channel(830401868596379648)
    
        embed =ebed("Member left")
        embed.add_field(name="Discord Tag",value=f"{member.name}#{member.discriminator}")
        await channel.send(embed=embed)

@bot.event
async def on_invite_create(invite):
    gid = invite.guild.id
    if gid == 829743733577744416:
        embed = ebed("Invite Created")
        embed.add_field(name="Created By:",value=f"{invite.inviter}",inline=True)
        embed.add_field(name="Url",value=f"{invite.url}",inline=False)
        embed.add_field(name="Time valid for",value=f"{invite.max_age} secs",inline=True)
        embed.add_field(name="Uses valid for",value=f"{invite.max_uses}",inline=True)
        embed.set_footer(text=f"Server: {invite.guild.name}",icon_url=f"{invite.guild.icon_url}")
        channel = bot.get_channel(830401868596379648)
        await channel.send(embed=embed)

@bot.command(name='gender')
async def gend(ctx,what=None):
    if what != None:
        check = gse(ctx.author.id)
        if check is None:
            what = str(what).lower()
            if what.startswith('m'):
                gender(ctx.author.id,'male')
                await ctx.send(f'{ctx.author.mention} your sex is set to Male')
            elif what.startswith('f'):
                gender(ctx.author.id,'female')
                await ctx.send(f'{ctx.author.mention} your sex is set to female')
            else:
                await ctx.send(f'{ctx.author.mention} your sex is set to {what}')
        else:
            await ctx.send('You gender is recorded to change it wait for 2 days')
    else:
        await ctx.send('Specify your gender like e.g `stacy gender female`')

@bot.command(name='RR', aliases=["russian_roullete"])
async def rr(ctx,who:discord.Member=None):
    await ctx.send("Russian Roullete \n1 bullet 6 slots \nYour life depends on your luck\nRespond with `Click` on your turn")
    nums = [1,2,3,4,5,6]
    bs = random.choice(nums)
    call = [1,0]
    start = random.choice(call)
    def check(m):
        return m.author.id == ctx.author.id
    def check2(m):
        return m.author.id == medit(who.edit).get('Member id')
    def over(x):
        return x == bs

    for x in nums:
        if start == 1:
            if who is None:
                await ctx.send('My Turn')
                await ctx.send('Click.!')
                o = over(x)
                start = 0
            else:
                await ctx.send(f"<@!{medit(who.edit).get('Member id')}> 's turn")
                try:
                    await bot.wait_for('message',check=check2, timeout=10)
                except asyncio.TimeoutError:
                    await ctx.send(f"Since You were not looking {ctx.author.mention} Pulled the trigger now you are ded")
                    return
                o = over(x)
                start = 0
        else:
            if who is None:
                await ctx.send('Your Turn')
            else:
                await ctx.send(f"{ctx.author.mention}'s turn")
            try:
                await bot.wait_for('message',check=check, timeout=10)
            except asyncio.TimeoutError:
                if who is None:
                    await ctx.send("Since You were not looking I Pulled the trigger now you are ded")
                else:
                    await ctx.send(f"Since You were not looking <@!{medit(who.edit).get('Member id')}> Pulled the trigger now you are ded")
                return
            o = over(x)
            start = 1
        
        if o == True:
            if start == 1:
                await ctx.send(f'Bang.! :boom:, {ctx.author.mention} is ded')
                return
            else:
                if who is None:
                    await ctx.send(f'Bang.! :boom:, <@!772381863528103966> is ded')
                else:
                    await ctx.send(f"Bang.! :boom:, <@{medit(who.edit).get('Member id')}> is ded")
                return
      
@bot.command(name='pp')
async def pp(ctx,who:discord.Member=None):
    if who is None:
        await ctx.send("Specify a member to use this command please")
        return
    if medit(who.edit).get('Member id') == 772381863528103966:
        await ctx.send("||Well I don't have one :face_with_hand_over_mouth: ||")
        return
    ches = [1,2,3,4,5,6,7,8,9,10,11,12]
    if ctx.author == 562553902990491659:
        ches = [6,7,8,9,10,11,12]
    lp = random.choice(ches)
    pp = "=" * lp
    if gse(medit(who.edit).get('Member id')) != None and gse(medit(who.edit).get('Member id'))[2] == "male":
        await ctx.send(f"""
        <@!{medit(who.edit).get('Member id')}>'s PP
        ||          8{pp}D          ||""")
    elif gse(medit(who.edit).get('Member id')) != None and gse(medit(who.edit).get('Member id'))[2] == "female":
        await ctx.send(f"""||Whoa girls don't have pp's they have hoha's do some research dum dum||""")
    elif gse(medit(who.edit).get('Member id')) is None:
        if ctx.author.id == medit(who.edit).get('Member id'):
            await ctx.send("Before using this command register your gender : use `stacy gender (your gender)`")
        else:
            await ctx.send(f"<@!{medit(who.edit).get('Member id')}> have not registered there gender ask them to use `stacy gender (his/her gender)`")

@bot.command(name='mute')
@commands.has_any_role('Owner','Stacy','Developer')
async def mute(ctx,who:discord.Member=None):
    if who is None:
        await ctx.send("Mention someone to mute")
        return
    Role = "MUTED"
    role = get(ctx.guild.roles, name=Role)
    await who.add_roles(role)
    await ctx.send('https://i.pinimg.com/736x/81/da/f8/81daf8311f01426b311249df11f36a33.jpg')

@mute.error
async def mute_error(ctx,error):
    if isinstance(error, commands.errors.MissingRole):
        embed = ebed("Missing Role")
        embed.add_field(name='Needed Role',value="Owner")
        embed.add_field(name='Suggestion',value="Don't do something you are not authorized to...",inline=False)
        await ctx.send(embed=embed)

@bot.command(name='unmute')
@commands.has_any_role('Owner','Stacy','Developer')
async def unmute(ctx,who:discord.Member=None):
    if who is None:
        await ctx.send("Mention someone to mute")
        return
    Role = "MUTED"
    role = get(ctx.guild.roles, name=Role)
    await who.remove_roles(role)
    await ctx.send(f'Unmuted {who.mention}')

@unmute.error
async def unmute_error(ctx,error):
    if isinstance(error, commands.errors.MissingRole):
        embed = ebed("Missing Role")
        embed.add_field(name='Needed Role',value="Owner")
        embed.add_field(name='Suggestion',value="Don't do something you are not authorized to...",inline=False)
        await ctx.send(embed=embed)

@bot.command(name='censor')
async def censor(ctx,*what):
    await ctx.message.delete()
    msg = ''
    for x in what:
        msg += f'{x} '
    
    await ctx.send(f'|| {msg} ||')
    
@bot.command(name='f-token', aliases=['ft','tkn'])
async def ftoken(ctx,what='help',who:discord.Member=None):
    def check(m):
        return m.author.id == medit(who.edit).get('Member id')
    if what == 'help':
        embed = ebed('F-token')
        embed.add_field(name='What is F-Token',value='F-Token is a digital token which is used \n when you have done something silly and now you are sorry for that action \n So if the other person has a F-token with your credential he/she have to forgive you no matter what')
        await ctx.send(embed=embed)
    elif what == 'balance':
        bal = fbal(ctx.author.id)
        embed = ebed('F-token Balance')
        embed.add_field(name='Balance', value=f'{bal}')
        await ctx.send(embed=embed)
    elif what == 'add':
        if who is None:
            await ctx.send('Enter a Member with whom you wanna get a f-token')
            return
        else:
            await ctx.send(f'{who.mention}, If you agree to give {ctx.author.mention} a f-token reply with \n add')
            cnf = await bot.wait_for('message',check=check,timeout=60)
            if str(cnf.content).capitalize() == 'Add':
                ftokn(ctx.author.id,medit(who.edit).get('Member id'))
                await ctx.send('Process Completed')
            else:
                await ctx.send('Process Aborted')
    elif what == 'own':
        if who is None:
            await ctx.send('Enter a Member with whom you wanna check your f-token balance')
            return
        else:
            if cbal(ctx.author.id,medit(who.edit).get('Member id')) == 0:
                use(ctx.author.id,medit(who.edit).get('Member id'))
                await ctx.send(f'{who.mention} please forgive {ctx.author.mention}')

@bot.command(name='xo',aliases=['ttt','tictactoe'])
async def TicTacToe(ctx,p2:discord.Member=None):
    def check(m):
        return m.author.id == ctx.author.id
    def check2(m):
        return m.author.id == medit(p2.edit).get('Member id')
    if p2 != None:
        await ctx.send("Do You accept the challenge\nYes/No")
        try:
            ready = await bot.wait_for('message',check=check2 , timeout=15)
        except asyncio.TimeoutError:
            await ctx.send("Timed Out")
            return
        if str(ready.content[0]).capitalize() != "Y":
            await ctx.send(f'{ctx.author.mention}, {p2.mention} did not accept the challenge')
            return

    slot = {1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9}

    nums = [0,1]
    slots = [1,2,3,4,5,6,7,8,9]
    turn = await ctx.send("To be decided")
    chance = random.choice(nums)
    slots_filled = []
    if p2 != None and p2.is_on_mobile():
        message = await ctx.send("""``` 1 | 2 | 3
 —————————
 4 | 5 | 6
 —————————
 7 | 8 | 9
 ```""")

    elif ctx.author.is_on_mobile():
        message = await ctx.send("""``` 1 | 2 | 3
 —————————
 4 | 5 | 6
 —————————
 7 | 8 | 9
 ```""")
    else:
        embed = ebed("Tic-Tac-Toe")
        embed.add_field(name="Slot 1",value=slot[1])
        embed.add_field(name="Slot 2",value=slot[2])
        embed.add_field(name="Slot 3",value=slot[3])
        embed.add_field(name="Slot 4",value=slot[4])
        embed.add_field(name="Slot 5",value=slot[5])
        embed.add_field(name="Slot 6",value=slot[6])
        embed.add_field(name="Slot 7",value=slot[7])
        embed.add_field(name="Slot 8",value=slot[8])
        embed.add_field(name="Slot 9",value=slot[9])
        message = await ctx.send(embed=embed)
    
    def symbol(chance):
        if ctx.author.is_on_mobile():
            if chance == 0:
                return "O"
            else:
                return "X"    
        else:
            if chance == 0:
                return ":o:"
            else:
                return ":x:"

    def tie():
        z = 0 
        for x in slots_filled:
            z += 1
        if z == 9:
            return True
        else:
            return False

    def victory():
        if slot[1] == slot[2] and slot[2] == slot[3]:
            return True
        elif slot[1] == slot[4] and slot[4] == slot[7]:
            return True
        elif slot[1] == slot[5] and slot[5] == slot[9]:
            return True
        elif slot[2] == slot[1] and slot[1] == slot[3]:
            return True
        elif slot[2] == slot[5] and slot[5] == slot[8]:
            return True
        elif slot[3] == slot[6] and slot[6] == slot[9]:
            return True
        elif slot[3] == slot[5] and slot[5] == slot[7]:
            return True
        elif slot[4] == slot[5] and slot[5] == slot[6]:
            return True
        elif slot[7] == slot[8] and slot[8] == slot[9]:
            return True
        elif slot[8] == slot[5] and slot[5] == slot[2]:
            return True
        elif tie():
            return "TIE"
        else:
            return False

    async def slot_check(inp:int,chance):
        if inp in slots_filled:
            await ctx.send('Slot Occupied select a different one')
            return False
        else:
            slots_filled.append(inp)
            slot[inp] = symbol(chance=chance)
            return True

    def bot_choice():
        inp = random.choice(slots)
        if inp in slots_filled:
            bot_choice()
        else:
            slots_filled.append(inp)
            slot[inp] = symbol(1)
            return True
    

    async def agaisnt_bot(chance):
        count = 1
        while count == 1:
            if chance == 0:
                await turn.edit(content=f"{ctx.author.mention}'s turn")
                try:
                    user_inp = await bot.wait_for('message',check=check, timeout=20)
                except asyncio.TimeoutError:
                    await ctx.send(f'{ctx.author.mention} lost due to timeout')
                    count = 0
                    return
                try:
                    inp = int(user_inp.content)
                    is_int = await slot_check(inp,0)
                except:
                    await ctx.send('Enter A Valid Input')
                    await agaisnt_bot(0)
                await user_inp.delete()
                if is_int:
                    if ctx.author.is_on_mobile():
                        await message.edit(content=f"""``` {slot[1]} | {slot[2]} | {slot[3]}
 —————————
 {slot[4]} | {slot[5]} | {slot[6]}
 —————————
 {slot[7]} | {slot[8]} | {slot[9]}
 ```""")
                    else:
                        board = ebed("Tic-Tac-Toe")
                        board.add_field(name="Slot 1",value=slot[1])
                        board.add_field(name="Slot 2",value=slot[2])
                        board.add_field(name="Slot 3",value=slot[3])
                        board.add_field(name="Slot 4",value=slot[4])
                        board.add_field(name="Slot 5",value=slot[5])
                        board.add_field(name="Slot 6",value=slot[6])
                        board.add_field(name="Slot 7",value=slot[7])
                        board.add_field(name="Slot 8",value=slot[8])
                        board.add_field(name="Slot 9",value=slot[9])
                        await message.edit(embed=board)
                    chance = 1
                    result = victory()
                    if result == True:
                        count = 0
                        await ctx.send(f'{ctx.author.mention} Won')
                    elif result == "TIE":
                        await ctx.send("It's a tie")
                        count = 0
                else:
                    await agaisnt_bot(0)
            else:
                
                await turn.edit(content="<@!772381863528103966>'s Turn")
                chance = 0
                inp = bot_choice()
                if ctx.author.is_on_mobile():
                    await message.edit(content=f"""``` {slot[1]} | {slot[2]} | {slot[3]}
 —————————
 {slot[4]} | {slot[5]} | {slot[6]}
 —————————
 {slot[7]} | {slot[8]} | {slot[9]}
 ```""")         
                else:
                    board = ebed("Tic-Tac-Toe")
                    board.add_field(name="Slot 1",value=slot[1])
                    board.add_field(name="Slot 2",value=slot[2])
                    board.add_field(name="Slot 3",value=slot[3])
                    board.add_field(name="Slot 4",value=slot[4])
                    board.add_field(name="Slot 5",value=slot[5])
                    board.add_field(name="Slot 6",value=slot[6])
                    board.add_field(name="Slot 7",value=slot[7])
                    board.add_field(name="Slot 8",value=slot[8])
                    board.add_field(name="Slot 9",value=slot[9])
                    await message.edit(embed=board)
                    
                result = victory()
                if result == True:
                    count = 0
                    await ctx.send(f'<@!772381863528103966> Won')
                elif result == "TIE":
                    await ctx.send("It's a tie")
                    count = 0

    async def against_human(chance):
        count = 1
        while count == 1:
            if chance == 0:
                await turn.edit(content=f"{ctx.author.mention}'s turn")
                try:
                    user_inp = await bot.wait_for('message',check=check, timeout=20)
                except asyncio.TimeoutError:
                    await ctx.send(f'{ctx.author.mention} lost due to timeout')
                    count = 0
                    return
                try:
                    inp = int(user_inp.content)
                    is_int = await slot_check(inp,0)
                except:
                    await ctx.send('Enter A Valid Input')
                    await against_human(0)
                await user_inp.delete()
                if is_int:
                    if ctx.author.is_on_mobile() or p2.is_on_mobile():
                        await message.edit(content=f"""``` {slot[1]} | {slot[2]} | {slot[3]}
 —————————
 {slot[4]} | {slot[5]} | {slot[6]}
 —————————
 {slot[7]} | {slot[8]} | {slot[9]}
 ```""")
                    else:
                        board = ebed("Tic-Tac-Toe")
                        board.add_field(name="Slot 1",value=slot[1])
                        board.add_field(name="Slot 2",value=slot[2])
                        board.add_field(name="Slot 3",value=slot[3])
                        board.add_field(name="Slot 4",value=slot[4])
                        board.add_field(name="Slot 5",value=slot[5])
                        board.add_field(name="Slot 6",value=slot[6])
                        board.add_field(name="Slot 7",value=slot[7])
                        board.add_field(name="Slot 8",value=slot[8])
                        board.add_field(name="Slot 9",value=slot[9])
                        await message.edit(embed=board)
                    chance = 1
                    result = victory()
                    if result == True:
                        count = 0
                        await ctx.send(f'{ctx.author.mention} Won')
                    elif result == "TIE":
                        await ctx.send("It's a tie")
                        count = 0
            else:
                await turn.edit(content=f"{p2.mention}'s turn")
                try:
                    user_inp = await bot.wait_for('message',check=check2, timeout=20)
                except asyncio.TimeoutError:
                    await ctx.send(f'{p2.mention} lost due to timeout')
                    count = 0
                    return
                try:
                    inp = int(user_inp.content)
                    is_int = await slot_check(inp,1)
                except:
                    await ctx.send('Enter A Valid Input')
                    await against_human(1)
                await user_inp.delete()
                if is_int:
                    if ctx.author.is_on_mobile() or p2.is_on_mobile():
                        await message.edit(content=f"""``` {slot[1]} | {slot[2]} | {slot[3]}
 —————————
 {slot[4]} | {slot[5]} | {slot[6]}
 —————————
 {slot[7]} | {slot[8]} | {slot[9]}
 ```""")
                    else:
                        board = ebed("Tic-Tac-Toe")
                        board.add_field(name="Slot 1",value=slot[1])
                        board.add_field(name="Slot 2",value=slot[2])
                        board.add_field(name="Slot 3",value=slot[3])
                        board.add_field(name="Slot 4",value=slot[4])
                        board.add_field(name="Slot 5",value=slot[5])
                        board.add_field(name="Slot 6",value=slot[6])
                        board.add_field(name="Slot 7",value=slot[7])
                        board.add_field(name="Slot 8",value=slot[8])
                        board.add_field(name="Slot 9",value=slot[9])
                        await message.edit(embed=board)
                    chance = 0
                    result = victory()
                    if result == True:
                        count = 0
                        await ctx.send(f'{p2.mention} Won')
                    elif result == "TIE":
                        await ctx.send("It's a tie")
                        count = 0

    if p2 is None:
        await ctx.send(f'Enter the number of the left slots on your turn')
        await agaisnt_bot(chance)
    else:
        await ctx.send(f'Enter the number of the left slots on your turn')
        await against_human(chance)
                    
@bot.command(name="akinator")
async def akin(ctx):
    
    

@bot.command(name="cricket", aliases=['Odd_even', 'hand_cricket', 'hc'])
async def cricket(ctx):
    def check(m):
        return m.author.id == ctx.author.id
    embed = Embed(title='Call',description='Call Odd or Even', color=color())
    embed.add_field(name="Call",value="To be decided", inline=False)
    embed.add_field(name="Your Number",value="Call odd/even first", inline=True)
    embed.add_field(name="Computer's Number",value="Will be decided",inline=True)
    embed.add_field(name="Task",value="Enter 'Odd' or 'Even'",inline=False)
    board = await ctx.send(embed=embed)
    p1 = ctx.author
    num = ["0","1","2","3","4","5","6","7","8","9","10"]
    opt = ["Bat","Ball"]

    def comp():
        nm = random.choice(num)
        return int(nm)

    async def toss_result(p1,call):
        a = 11
        while(a > 10):
            try:
                inp = await bot.wait_for('message', check=check, timeout=30)
            except asyncio.TimeoutError:
                await ctx.send("Timed out")
                return
            
            
            a = int(inp.content)
            await inp.delete()
            if a > 10 or type(a) != int or a < 0:
                await ctx.send("A Number between 1 and 10")
                a = 11

        cpi = comp()
        

        total = a + cpi

        if str(call).lower().startswith('e'):
            if total % 2 == 0:
                embed = Embed(title='Call',description='Call Odd or Even', color=color())
                embed.add_field(name="Call",value=f"{call}", inline=False)
                embed.add_field(name="Your Number",value=f"{a}", inline=True)
                embed.add_field(name="Computer's Number",value=f"{cpi}",inline=True)
                embed.add_field(name= "Result",value=f'{p1.name} won the toss')
                embed.add_field(name="Task",value="Choose to 'Bat' or 'Ball'",inline=False)
                await board.edit(embed=embed)

                return 1
            else:
                embed = Embed(title='Call',description='Call Odd or Even', color=color())
                embed.add_field(name="Call",value=f"{call}", inline=False)
                embed.add_field(name="Your Number",value=f"{a}", inline=True)
                embed.add_field(name="Computer's Number",value=f"{cpi}",inline=True)
                embed.add_field(name= "Result",value=f'Stacy won the toss')
                embed.add_field(name="Task",value="Choose to 'Bat' or 'Ball'",inline=False)
                await board.edit(embed=embed)
                return 0

        elif str(call).lower().startswith('o'):
            if total % 2 == 1:
                embed = Embed(title='Call',description='Call Odd or Even', color=color())
                embed.add_field(name="Call",value=f"{call}", inline=False)
                embed.add_field(name="Your Number",value=f"{a}", inline=True)
                embed.add_field(name="Computer's Number",value=f"{cpi}",inline=True)
                embed.add_field(name= "Result",value=f'{p1.name} won the toss')
                embed.add_field(name="Task",value="Choose to 'Bat' or 'Ball'",inline=False)
                await board.edit(embed=embed)
                return 1
            else:
                embed = Embed(title='Call',description='Call Odd or Even', color=color())
                embed.add_field(name="Call",value=f"{call}", inline=False)
                embed.add_field(name="Your Number",value=f"{a}", inline=True)
                embed.add_field(name="Computer's Number",value=f"{cpi}",inline=True)
                embed.add_field(name= "Result",value=f'Stacy won the toss')
                embed.add_field(name="Task",value="Choose to 'Bat' or 'Ball'",inline=False)
                await board.edit(embed=embed) 
                return 0

    async def choice(r,p1):
        if r == 1:
            embed = Embed(title='Call',description='Call Bat or ball', color=color())
            embed.add_field(name= "Result",value=f'{p1.nick} won the toss')
            embed.add_field(name="Choice",value=f"Waiting for call",inline=False)
            await board.edit(embed=embed)
            c = "ch"
            while(c=="ch"):
                try:
                    chie = await bot.wait_for('message',check=check,timeout=30)
                    k = chie
                    chie = str(chie.content).capitalize()
                    await k.delete()
                except asyncio.TimeoutError:
                    await ctx.send("Timed Out")

                if chie == "Bat" or chie == "Ball":
                    c = "ok"
                    embed = Embed(title='Call',description='Call Bat or ball', color=color())
                    embed.add_field(name= "Result",value=f'{p1.nick} won the toss')
                    embed.add_field(name="Choice",value=f"{chie}",inline=False)
                    await board.edit(embed=embed)
                    return chie
                else:
                    await board.edit(content="Bat or Ball \nIt's simple")
        else:
            chie = random.choice(opt)
            embed = Embed(title='Call',description='Call Bat or ball', color=color())
            embed.add_field(name= "Result",value=f'Stacy won the toss')
            embed.add_field(name="Choice",value=f"{chie}",inline=False)
            await board.edit(embed=embed)
            return chie

    async def bat_ball(who,p1,i1:int):
        status = "NO"
        run = 0
        if who == p1:
            while(status == "NO"):
                try:
                    hand = await bot.wait_for('message',check=check,timeout=30)
                    k = hand
                    try:
                        hand = int(hand.content)
                    except:
                        await board.edit(content="Enter a valid integer next time I'm recording this hand as a 0")
                        hand = 0
                    await k.delete()
                except:
                    await ctx.send("Timed out")
                    return

                chand = comp()

                if chand == hand:
                    status = "out"
                    
                    embed = Embed(title='Game',description='This is the game board', color=color())
                    embed.add_field(name="Batting",value=f"{p1.nick}", inline=True)
                    embed.add_field(name="Bowling",value=f"Stacy",inline=True)
                    embed.add_field(name="Your Hand",value=f"{hand}", inline=False)
                    embed.add_field(name="Computer's hand",value=f"{chand}",inline=False)
                    if i1 == 99999999999:
                        embed.add_field(name="Target",value=f"To be set",inline=True)
                    else:
                        embed.add_field(name="Target",value=f"{i1}",inline=True)
                    embed.add_field(name= "Batting_Score",value=f'{run}',inline=True)
                    await board.edit(embed=embed,content="OUT!!")
                    return int(run)
                elif hand >= 0 and hand <=10:
                    run += hand
                    embed = Embed(title='Game',description='This is the game board', color=color())
                    embed.add_field(name="Batting",value=f"{p1.nick}", inline=True)
                    embed.add_field(name="Bowling",value=f"Stacy",inline=True)
                    embed.add_field(name="Your Hand",value=f"{hand}", inline=False)
                    embed.add_field(name="Computer's hand",value=f"{chand}",inline=False)
                    if i1 == 99999999999:
                        embed.add_field(name="Target",value=f"To be set",inline=True)
                    else:
                        embed.add_field(name="Target",value=f"{i1}",inline=True)
                    embed.add_field(name= "Batting_Score",value=f'{run}',inline=True)
                    await board.edit(embed=embed,content="Enter a number in range 0 to 10")
                    if i1 < run:
                        status = "won"
                        return run
                else:
                    await board.edit(content="What part of 'between 1 and 10' you don't get?")
        else:
            while(status == "NO"):
                try:
                    hand = await bot.wait_for('message',check=check,timeout=30)
                    k = hand
                    try:
                        hand = int(hand.content)
                    except:
                        await board.edit(content="Enter a valid integer next time I'm recording this hand as a 0")
                        hand = 0
                    await k.delete()
                except:
                    await ctx.send("Timed out")
                    return
                chand = comp()

                if chand == hand:
                    status = "out"
                    embed = Embed(title='Game',description='This is the game board', color=color())
                    embed.add_field(name="Batting",value=f"Stacy", inline=True)
                    embed.add_field(name="Bowling",value=f"{p1.nick}",inline=True)
                    embed.add_field(name="Your Hand",value=f"{hand}", inline=False)
                    embed.add_field(name="Computer's hand",value=f"{chand}",inline=False)
                    if i1 == 99999999999:
                        embed.add_field(name="Target",value=f"To be set",inline=True)
                    else:
                        embed.add_field(name="Target",value=f"{i1}",inline=True)
                    embed.add_field(name= "Batting_Score",value=f'{run}',inline=True)
                    await board.edit(embed=embed,content="OUT!!")
                    return int(run)
                elif chand >= 0 and chand <= 10:
                    run += chand
                    embed = Embed(title='Game',description='This is the game board', color=color())
                    embed.add_field(name="Batting",value=f"Stacy", inline=True)
                    embed.add_field(name="Bowling",value=f"{p1.nick}",inline=True)
                    embed.add_field(name="Your Hand",value=f"{hand}", inline=False)
                    embed.add_field(name="Computer's hand",value=f"{chand}",inline=False)
                    if i1 == 99999999999:
                        embed.add_field(name="Target",value=f"To be set",inline=True)
                    else:
                        embed.add_field(name="Target",value=f"{i1}",inline=True)
                    embed.add_field(name= "Batting_Score",value=f'{run}',inline=True)
                    await board.edit(embed=embed,content="Enter a number in range 0 to 10")
                    if i1 < run:
                        status = "won"
                        return run
                else:
                    await board.edit(content="What part of 'between 1 and 10' you don't get?")

    def result(sp,sc):
        if sp == sc:
            return "Tie"
        elif sp > sc:
            return "You Won"
        else:
            return "You Lost"



    try:
        toss = await bot.wait_for('message',check=check,timeout=30)
        if str(toss.content).lower().startswith('e') or str(toss.content).lower().startswith('o'):
            if str(toss.content).lower().startswith('e'):
                call = "Even"
                await toss.delete()
            else:
                call = "Odd"
                await toss.delete()
        else:
            await ctx.send("Enter Odd or Even only")
            await cricket(ctx)
    except asyncio.TimeoutError:
        await ctx.send("Timed Out")
        return

    embed = Embed(title='Call',description='Call Odd or Even', color=color())
    embed.add_field(name="Call",value=f"{call}", inline=False)
    embed.add_field(name="Your Number",value="Waiting for input", inline=True)
    embed.add_field(name="Computer's Number",value="Will be decided",inline=True)
    embed.add_field(name="Task",value="Enter a number in range of 0 to 10",inline=False)
    await board.edit(embed=embed)

    r = await toss_result(p1,toss.content)
    
    bob = await choice(r,p1)

    inn = "Running"
    while(inn == "Running"):
        if r == 1:
            if bob == "Bat":
                embed = Embed(title='Game',description='This is the game board', color=color())
                embed.add_field(name="Batting",value=f"{p1.nick}", inline=True)
                embed.add_field(name="Bowling",value=f"Stacy",inline=True)
                embed.add_field(name="Your Hand",value=f"0", inline=False)
                embed.add_field(name="Computer's hand",value=f"0",inline=False)
                embed.add_field(name="Target",value="To be set",inline=True)
                embed.add_field(name= "Result",value=f'Match is going on',inline=True)
                await board.edit(embed=embed,content="Enter a Number in range 0 to 10")
                runs = await bat_ball(p1,p1,99999999999)
                embed = Embed(title='Game',description='This is the game board', color=color())
                embed.add_field(name="Batting",value=f"Stacy", inline=True)
                embed.add_field(name="Bowling",value=f"{p1.nick}",inline=True)
                embed.add_field(name="Your Hand",value=f"0", inline=False)
                embed.add_field(name="Computer's hand",value=f"0",inline=False)
                embed.add_field(name="Target",value=f"{runs}",inline=True)
                embed.add_field(name= "Result",value=f'Match is going on',inline=True)
                await board.edit(embed=embed,content="Enter a Number in range 0 to 10")
                cruns = await bat_ball("comp",p1,runs)
            else:
                cruns = await bat_ball("comp",p1,99999999999)
                embed = Embed(title='Game',description='This is the game board', color=color())
                embed.add_field(name="Batting",value=f"Stacy", inline=True)
                embed.add_field(name="Bowling",value=f"{p1.nick}",inline=True)
                embed.add_field(name="Your Hand",value=f"0", inline=False)
                embed.add_field(name="Computer's hand",value=f"0",inline=False)
                embed.add_field(name="Target",value=f"To be set",inline=True)
                embed.add_field(name= "Result",value=f'Match is going on',inline=True)
                await board.edit(embed=embed,content="Enter a Number in range 0 to 10")
                embed = Embed(title='Game',description='This is the game board', color=color())
                embed.add_field(name="Batting",value=f"{p1.nick}", inline=True)
                embed.add_field(name="Bowling",value=f"Stacy",inline=True)
                embed.add_field(name="Your Hand",value=f"0", inline=False)
                embed.add_field(name="Computer's hand",value=f"0",inline=False)
                embed.add_field(name="Target",value=f"{cruns}",inline=True)
                embed.add_field(name= "Result",value=f'Match is going on',inline=True)
                await board.edit(embed=embed,content="Enter a Number in range 0 to 10")
                runs = await bat_ball(p1,p1,cruns)
        else:
            if bob == "Bat":
                embed = Embed(title='Game',description='This is the game board', color=color())
                embed.add_field(name="Batting",value=f"Stacy", inline=True)
                embed.add_field(name="Bowling",value=f"{p1.nick}",inline=True)
                embed.add_field(name="Your Hand",value=f"0", inline=False)
                embed.add_field(name="Computer's hand",value=f"0",inline=False)
                embed.add_field(name="Target",value=f"To be set",inline=True)
                embed.add_field(name= "Result",value=f'Match is going on',inline=True)
                await board.edit(embed=embed,content="Enter a Number in range 0 to 10")
                cruns = await bat_ball("comp",p1,99999999999)
                embed = Embed(title='Game',description='This is the game board', color=color())
                embed.add_field(name="Batting",value=f"{p1.nick}", inline=True)
                embed.add_field(name="Bowling",value=f"Stacy",inline=True)
                embed.add_field(name="Your Hand",value=f"0", inline=False)
                embed.add_field(name="Computer's hand",value=f"0",inline=False)
                embed.add_field(name="Target",value=f"{cruns}",inline=True)
                embed.add_field(name= "Result",value=f'Match is going on',inline=True)
                await board.edit(embed=embed,content="Enter a Number in range 0 to 10")
                runs = await bat_ball(p1,p1,cruns)
            else:
                embed = Embed(title='Game',description='This is the game board', color=color())
                embed.add_field(name="Batting",value=f"{p1.nick}", inline=True)
                embed.add_field(name="Bowling",value=f"Stacy",inline=True)
                embed.add_field(name="Your Hand",value=f"0", inline=False)
                embed.add_field(name="Computer's hand",value=f"0",inline=False)
                embed.add_field(name="Target",value=f"To be set",inline=True)
                embed.add_field(name= "Result",value=f'Match is going on',inline=True)
                await board.edit(embed=embed,content="Enter a Number in range 0 to 10")
                runs = await bat_ball(p1,p1,99999999999)
                embed = Embed(title='Game',description='This is the game board', color=color())
                embed.add_field(name="Batting",value=f"Stacy", inline=True)
                embed.add_field(name="Bowling",value=f"{p1.nick}",inline=True)
                embed.add_field(name="Your Hand",value=f"0", inline=False)
                embed.add_field(name="Computer's hand",value=f"0",inline=False)
                embed.add_field(name="Target",value=f"{runs}",inline=True)
                embed.add_field(name= "Result",value=f'Match is going on',inline=True)
                await board.edit(embed=embed,content="Enter a Number in range 0 to 10")
                cruns = await bat_ball("comp",p1,runs)
        outcome = result(runs,cruns)
        embed = Embed(title='Game',description='This is the game board', color=color())
        embed.add_field(name="Batting",value=f"Match Completed", inline=True)
        embed.add_field(name="Bowling",value=f"Match Completed",inline=True)
        embed.add_field(name= "Result",value=f'{outcome}',inline=True)
        await board.edit(embed=embed)
        inn = "done"
    
@bot.command(name="poll")
async def poll(ctx,*what):
    w = 0
    des = ""
    for word in what:
        if w > 3:
            des += f"{word} "
        w += 1
        
    des = des.rstrip()
    z = 0
    reaction = ['1️⃣','2️⃣','3️⃣']
    if ctx.author.nick != None:
        by = ctx.author.nick
    else:
        by = ctx.author.name
    embed = Embed(title="Poll",description=f"This is poll created by {by} for {des}",color=color())
    for x in what:
        z += 1
        if z < 4:
            embed.add_field(name=f"Option {z}",value=f"{x}")
    poll = await ctx.send(embed=embed)
    for x in range(0,z):
        if x <= 2:
            emoji = reaction[x]
            await poll.add_reaction(emoji)

@bot.command(name="hangman")
async def hangman(ctx):
    pass

@bot.command(name="card",aliases=["cards"])
async def ncards(ctx, what=None):
    data = {'eyes':{'name':'Unsend the shit Right Now','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0001.jpg'},
    'booty':{'name':'That booty seems kinda sus ngl','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0002.jpg'},
    'stfu':{'name':'Will you shut up man','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0003.jpg'},
    'nword':{'name':'You are now free to use the N word in this server','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0004.jpg'},
    'rib_gone':{'name':'You got wierd taste ngl','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0005.jpg'},
    'dafaq':{'name':'What are you talking bout...','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0006.jpg'},
    'like_me':{'name':'Like me now spell card','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0009.jpg'},
    'simp':{'name':f"Looks like {ctx.author.nick} is a professional simp",'url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0010.jpg'},
    'ass':{'name':'I wanna see dat ass now','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0011.jpg'},
    'tits':{'name':'YOUR BOOBIES PLS....','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0012.jpg'},
    'only_me':{'name':f'Talk to {ctx.author.nick} only','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0013.jpg'},
    'truth':{'name':'Must tell me everthing spell','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0014.jpg'},
    'silence_wench':{'name':'Spell card No more horny','url':'https://oughtenf.sirv.com/Cards/niggalationcards-20210114-0016.jpg'},
    'be_my_religion':{'name':"I'd love to...",'url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0017.jpg'},
    'bold_voice':{'name':'listen here you b*tch...','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0022.jpg'},
    'we_are_bros':{'name':'No homo...','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0023.jpg'},
    'cozy_soapy':{'name':'I need a soapy pic right now....','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0027.jpg'},
    'the_nip_tip':{'name':'Babe just the nip tip','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0028.jpg'},
    'oh_really':{'name':'Oh Really.......','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0030.jpg'},
    'liar':{'name':'Hehe ehy you lying...','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0032.jpg'},
    'dafaq_wrong':{'name':"What's wrong with you...",'url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0033.jpg'},
    'she_so_hawt':{'name':'I like you but Damn.... she fine','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0034.jpg'},
    'unwrap_her':{'name':'The pics in the DM right now','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0037.jpg'},
    'camel_toe':{'name':"Your call girl...",'url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0038.jpg'},
    'no_come_back':{'name':'How does it feel....','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0047.jpg'},
    'low_power':{'name':'Yo gotta charge my cell.. byee','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0048.jpg'},
    'no_shit':{'name':'Oh my god... The all knowing','url':'http://oughtenf.sirv.com/Cards/niggalationcards-20210114-0049.jpg'}}

    if what is None:
        string = ''
        for x in data:
            string += f'{x} /'
        embed = Embed(title ="List of all cards available",color=color())
        embed.add_field(name="Cards",value=string)
        await ctx.send(embed=embed)
        return
    else:
        if what in data:
            name = data.get(str(what)).get('name')
            url = data.get(str(what)).get('url')
            embed = ebed(name)
            embed.set_image(url=url)
            await ctx.send(embed=embed)
        else:
            await ctx.send("That card doesn't exist in my database \n try one from the following")
            await ncards(ctx,what=None)
    
@bot.command(name="stars")
async def stars(ctx,pattern,num):
    num = int(num)
    pattern = int(pattern)

    async def stars1(ctx,num:int):
        for x in range(1,num+1):
            await ctx.send(f"`{'*'*x}`")
        return

    async def stars2(ctx,num:int):
        for x in range(1,num+1):
            await ctx.send(f"`{' '*(num-x)}{'*'*x}`")
        return

    async def stars3(ctx,num:int):
        if num % 2 == 0:
            k = "even"
        else:
            k = "odd"

        lines = num

        if k == "odd":
            space = int((num+1)/2)
            for x in range(1,num+1,2):
                await ctx.send(f"`{' '*(space-1)}{'*'*x}{' '*(space-1)}`")
                space = space - 1
        else:
            space = int((num)/2)
            for x in range(1,num+1,2):
                await ctx.send(f"`{' '*(space-1)}{'*'*(x+1)}{' '*(space-1)}`")
                space = space - 1
        return

    async def stars4(ctx,num:int):    
        space = int(num)
        z = 0
        for x in range(1,num+1):
            await ctx.send(f"`{' '*space}{'*'*(x+z)}{' '*space}`")
            z += 1
            space -= 1
        return
        
    if pattern < 5:
        if pattern == 1:
            await stars1(ctx,num)
        elif pattern == 2:
            await stars2(ctx,num)
        elif pattern == 3:
            await stars3(ctx,num)
        elif pattern == 4:
            await stars4(ctx,num)
        
@bot.command(name="bonk")
async def bonk(ctx,who:discord.Member=None):
    if medit(who.edit).get('Member id') == 562553902990491659:
        l = ctx.author.name
    else:
        l = who.name
    
    mid = "237388506"
    t2 = f"{l}"
    z = memegen(mid," ",t2)
    embed = ebed(f"{who.name} *BONK*")
    embed.set_image(url=z)
    await ctx.send(embed=embed, content=f"{who.mention}") 
    
@bot.command(name="nsfw", aliases=['ecchi'])
async def ecchi(ctx,*,cat=None):
    def check(reaction, user):
        return user == ctx.message.author and str(reaction.emoji) == '⬆️'

    if cat != None:
        if str(cat).lower().startswith("cam") or str(cat).lower().startswith("stream"):
            subreds = dict_call("nsfw").get("camera")
        elif str(cat).lower().startswith("milf"):
            subreds = dict_call("nsfw").get("milf")
        elif str(cat).lower().startswith("thigh"):
            subreds = dict_call("nsfw").get("thigh")
        elif str(cat).lower().startswith("boob") or str(cat).lower().startswith("bob") or str(cat).lower().startswith("tit") or str(cat).lower().startswith("nip") or str(cat).lower().startswith("tip"):
            subreds = dict_call("nsfw").get("boobs")
        elif str(cat).lower().startswith("yoga"):
            subreds = dict_call("nsfw").get("yoga")
        elif str(cat).lower().startswith("cos"):
            subreds = dict_call("nsfw").get("cosplay")
        elif str(cat).lower().startswith("puss"):
            subreds = dict_call("nsfw").get("coochie")
        elif str(cat).lower().startswith("bdsm"):
            subreds = dict_call("nsfw").get("bdsm")
        elif str(cat).lower().startswith("anal") or str(cat).lower().startswith("butt") or  str(cat).lower().startswith("booty"):
            subreds = dict_call("nsfw").get("butt")
        elif str(cat).lower().startswith("latina"):
            subreds = dict_call("nsfw").get("latina")
        elif str(cat).lower().startswith("les"):
            subreds = dict_call("nsfw").get("lesbian")
        elif str(cat).lower().startswith("red"):            
            subreds= dict_call("nsfw").get("redhead")
        elif str(cat).lower().startswith("blond"):            
            subreds= dict_call("nsfw").get("blonde")
        elif str(cat).lower().startswith("bru"):            
            subreds= dict_call("nsfw").get("brunette")
        elif str(cat).lower().startswith("ru"):            
            subreds=dict_call("nsfw").get("russian")
        elif str(cat).lower().startswith("ind") or str(cat).lower().startswith("bhabhi") :
            subreds=dict_call("nsfw").get("indian")
        elif str(cat).lower().startswith("asia"):            
            subreds=dict_call("nsfw").get("asian")
        elif str(cat).lower().startswith("douj"):
            subreds=dict_call("nsfw").get("douj")
        else:
            if ctx.channel.is_nsfw():
                params = {
                    'page':'dapi',
                    's':'post',
                    'q':'index',
                    'limit':1000,
                    'tags':cat
                }
                import re
                response = requests.get(url=dict_call("nsfw").get("url"),params=params)
                img_urls = []
                response = response.text
                number = len(re.findall('sample_url',response))

                for links in range(number):
                    response = response[response.find('sample_url')+len('sample_url":'):]
                    img_url = response[:response.find('"')]
                    img_urls.append(img_url)

                try:
                    url = random.choice(img_urls)

                    if not url.endswith('.mp4'):
                        em = ebed(str(cat).capitalize())
                        em.set_image(url=url)
                        message = await ctx.send(embed=em)
                        await message.add_reaction('⬆️')
                        await message.add_reaction('⬇️')
                    else:
                        message = await ctx.send(url)
                        await message.add_reaction('⬆️')
                        await message.add_reaction('⬇️')
                except:
                    await ctx.send(embed=ebed(f"No Results Found For : {cat}"))

                return
    else:
        subreds = dict_call("nsfw").get("all")
        

    if ctx.channel.is_nsfw():
        z = random.choice(subreds)
        try:
            subreddit = await reddit.subreddit(z)
            await subreddit.subscribe()
            all_subs=[]
            async for new in subreddit.top(limit=69):
                all_subs.append(new)
            k = 1
            t = 0
            r = 0
            message = await ctx.send(embed=ebed("Searching..."))
            while k==1:
                random_sub= random.choice(all_subs)
                if t <= 5:
                    await message.edit(embed=ebed(f"Looking at {random_sub}"))

                url= random_sub.url
                if (url.startswith("https://i.redd.it") or url.startswith("https://i.imgur")) and (url.endswith(".jpg") or url.endswith(".png") or url.endswith(".jpeg") or url.endswith(".gif")):
                    name = random_sub.title
                    k = 0
                if t < 50:
                    t += 1
                else:
                    await ctx.send("A moment....")
                    async for new in subreddit.hot(limit=50):
                        all_subs.append(new)
                    t = 0
                    if r == 0:
                        r +=1
                    else:
                        await message.edit(embed= ebed(f"Sorry, Unable to find something dark in that category : sauce = {subreddit}"))
                        k = 0
                        return

            em = ebed(name)
            em.set_image(url=url)
            em.set_footer(text=f"Sauce : {subreddit} || {random_sub} ")
            await message.edit(embed=em)
            await message.add_reaction('⬆️')
            await message.add_reaction('⬇️')
            try:
                emoji = await bot.wait_for('reaction_add', check=check, timeout=30)
                emoji = '⬆️'
            except:
                emoji = '⬇️'

            try:
                if emoji != '⬇️':
                    await random_sub.upvote()
                else:
                    await random_sub.downvote()        
            except:
                pass
        except:
            await ctx.send(embed=ebed(f'{z} is a private sub plaese remove it from the dictionary'))
    else:
        await ctx.send(embed = ebed("You need to use this command in a NSFW channel!"))

@bot.command(name="darkjoke", aliases=['dj'])
async def darkjoke(ctx):
    def check(reaction, user):
        return user == ctx.message.author and str(reaction.emoji) == '⬆️'

    if ctx.channel.is_nsfw() or ctx.message.guild.id == 395550925340540930:
        sub = random.choice(dict_call("nsfw").get("darkjoke"))
        subreddit = await reddit.subreddit(sub)
        await subreddit.subscribe()
        all_subs=[]
        async for new in subreddit.new(limit=50):
            all_subs.append(new)
        k = 1
        t = 0
        r = 0
        message = await ctx.send(embed=ebed("Searching..."))
        while k==1:
            random_sub= random.choice(all_subs)
            if t <= 5:
                await message.edit(embed=ebed(f"Looking at {random_sub}"))
            if random_sub.is_self:  # We only want to work without link posts
                text = random_sub.selftext
                name = random_sub.title
                k = 0

            if t < 50:
                t += 1
            else:
                await ctx.send("A moment....")
                async for new in subreddit.top(limit=50):
                    all_subs.append(new)
                t = 0
                if r == 0:
                    r +=1
                else:
                    await message.edit(embed= ebed(f"Sorry, Unable to find something ecchi in that category : sauce = {subreddit}"))
                    k = 0
                    return
        l = len(f"{text.split()[0]} {text.split()[1]}")
        try:
            text.split()[2]
        except:
            l = len(f"{text.split()[0]}")
                
        em = ebed(name)
        em.add_field(name=f"{text.split()[0]} {text.split()[1]}",value=f"{text[l::]}",inline=False)
        em.set_footer(text=f"Sauce : {subreddit} || {random_sub} ")
        await ctx.message.delete()
        await message.edit(embed=em)
        await message.add_reaction('⬆️')
        await message.add_reaction('⬇️')
        try:
            emoji = await bot.wait_for('reaction_add', check=check, timeout=30)
            emoji = '⬆️'
        except:
            emoji = '⬇️'
        
        try:
            if emoji != '⬇️':
                await random_sub.upvote()
            else:
                await random_sub.downvote()        
        except:
            pass
    else:
        await ctx.send(embed = ebed("You need to use this command in a NSFW channel!"))

@bot.command(name="nsfw_video",aliases=['nv'])
async def nsfw_video(ctx):
    def check(reaction, user):
        return user == ctx.message.author and str(reaction.emoji) == '⬆️'

    subreds = dict_call("nsfw").get("video")
    if ctx.channel.is_nsfw():
        z = random.choice(subreds)
        try:
            subreddit = await reddit.subreddit(z)
            await subreddit.subscribe()
            all_subs=[]
            async for new in subreddit.new(limit=50):
                all_subs.append(new)
            k = 1
            t = 0
            r = 0
            message = await ctx.send(embed=ebed("Searching..."))
            while k==1:
                random_sub= random.choice(all_subs)
                url= random_sub.url
                if t <= 5:
                    await message.edit(embed=ebed(f"Looking at {random_sub}"))
                    print(url)
                    print(random_sub.is_video)
                #and (url.endswith(".jpg") or url.endswith(".png") or url.endswith(".jpeg") or url.endswith(".gif"))
                if  url.startswith("https://www.redgifs") :
                    name = random_sub.title
                    k = 0
                if t < 50:
                    t += 1
                else:
                    await ctx.send("A moment....")
                    async for new in subreddit.top(limit=50):
                        all_subs.append(new)
                    t = 0
                    if r == 0:
                        r +=1
                    else:
                        await message.edit(embed= ebed(f"Sorry, Unable to find something dark in that category : sauce = {subreddit}"))
                        k = 0
                        return
            await message.edit(content=url,embed=None)
            await message.add_reaction('⬆️')
            await message.add_reaction('⬇️')
            try:
                emoji = await bot.wait_for('reaction_add', check=check, timeout=30)
                emoji = '⬆️'
            except:
                emoji = '⬇️'

            try:
                if emoji != '⬇️':
                    await random_sub.upvote()
                else:
                    await random_sub.downvote()        
            except:
                pass
        except:
            await ctx.send(embed=ebed(f'{z} is a private sub plaese remove it from the dictionary'))
    else:
        await ctx.send(embed = ebed("You need to use this command in a NSFW channel!"))

@bot.command(name="darkmeme", aliases=['dm'])
async def darkmeme(ctx):
    def check(reaction, user):
        return user == ctx.message.author and str(reaction.emoji) == '⬆️'

    if ctx.channel.is_nsfw() or ctx.message.guild.id == 395550925340540930:
        sub = random.choice(dict_call("nsfw").get("darkmeme"))
        subreddit = await reddit.subreddit(sub)
        await subreddit.subscribe()
        all_subs=[]
        async for new in subreddit.top(limit=69):
            all_subs.append(new)
        k = 1
        t = 0
        r = 0
        message = await ctx.send(embed=ebed("Searching..."))
        while k==1:
            random_sub= random.choice(all_subs)
            if t <= 5:
                await message.edit(embed=ebed(f"Looking at {random_sub}"))
              # We only want to work with link posts
            try:
                text = random_sub.selftext
            except:
                text = None
            url = random_sub.url
            if (url.startswith("https://i.redd.it") or url.startswith("https://i.imgur")) and (url.endswith(".jpg") or url.endswith(".png") or url.endswith(".jpeg") or url.endswith(".gif")) :
                name = random_sub.title
                k = 0

            if t < 50:
                t += 1
            else:
                await ctx.send("A moment....")
                async for new in subreddit.new(limit=69):
                    all_subs.append(new)
                t = 0
                if r == 0:
                    r +=1
                else:
                    await message.edit(embed= ebed(f"Sorry, Unable to find something dark in that category : sauce = {subreddit}"))
                    k = 0
                    return
        em = ebed(name)

        if text != None:
            try:
                l = len(f"{text.split()[0]} {text.split()[1]}")
                try:
                    text.split()[2]
                except:
                    l = len(f"{text.split()[0]}")
                em.add_field(name=f"{text.split()[0]} {text.split()[1]}",value=f"{text[l::]}",inline=False)
            except:
                pass

        em.set_image(url=url)
        em.set_footer(text=f"Sauce : {subreddit} || {random_sub} ")
        await ctx.message.delete()
        await message.edit(embed=em)
        await message.add_reaction('⬆️')
        await message.add_reaction('⬇️')
        try:
            emoji = await bot.wait_for('reaction_add', check=check, timeout=10)
            emoji = '⬆️'
        except:
            emoji = '⬇️'
        
        try:
            if emoji != '⬇️':
                await random_sub.upvote()
            else:
                await random_sub.downvote()        
        except:
            pass
    else:
        await ctx.send(embed = ebed("You need to use this command in a NSFW channel!"))

@bot.command(name="wiki",aliases=["wikipedia"])
async def wikipedia(ctx,*,what=None):
    wiki_wiki = wikipediaapi.Wikipedia('en')

    page_py = wiki_wiki.page(f'{what}')

    await ctx.send(page_py.fullurl)

@bot.command(name="text")
async def text_spam(ctx):
    di = 838877294118174741
    di2 = 839572744777105419
    

    channel = bot.get_channel(di)
    channel2 = bot.get_channel(di2)
    z = 421
    a = 5
    for number in range(a):
        for numbers in range(z):
            await channel.send(numbers)
            await channel2.send(numbers)
            await asyncio.sleep(1)

@bot.command()
async def ping(ctx):
    await ctx.send(f'Pong! {round(bot.latency*1000, 1)} ms')

bot.run(token)  
