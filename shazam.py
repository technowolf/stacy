import requests
from passwd_dict import dict_call


url = "https://shazam.p.rapidapi.com/songs/list-artist-top-tracks"

querystring = {"id":"40008598","locale":"en-US"}

headers = {
    'x-rapidapi-key': f"{dict_call('Shazam')}",
    'x-rapidapi-host': "shazam.p.rapidapi.com"
    }

response = requests.request("GET", url, headers=headers, params=querystring).json()
x = 0
for y in response.get('tracks'):
    print(response.get('tracks')[x].get('title'))
    x+=1