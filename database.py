import sqlite3
import ast
import datetime

conn = sqlite3.connect('stacy.db')

def call(what):
    c = conn.cursor()
    c.execute(f'''SELECT * FROM {what}''')
    z = 0
    for x in c.fetchall():
        z += 1
    return z

def scall(did):
    c = conn.cursor()
    c.execute(f'''SELECT * FROM warnings WHERE wf = {did}''')
    z = 0
    for x in c.fetchall():
        z += 1
    return z
    
def add(warn,isf):
    c = conn.cursor()
    c.executemany(f"INSERT INTO warnings (warning, wf) VALUES (?,?)",[(warn,isf)])
    conn.commit()

def suges(sug,isb):
    c = conn.cursor()
    c.executemany(f"INSERT INTO suggestions (sug, by) VALUES (?,?)",[(sug,isb)])
    conn.commit()

def buggy(bug,isb):
    c = conn.cursor()
    c.executemany(f"INSERT INTO bugs (by, bissue) VALUES (?,?)",[(isb, bug)])
    conn.commit()

def gender(who, sex):
    c = conn.cursor()
    c.executemany(f"INSERT INTO gender (uid, sex) VALUES (?,?)",[(who, sex)])
    conn.commit()

def gse(who):
    c = conn.cursor()
    c.execute(f'''SELECT * FROM gender WHERE uid = {who}''')
    k = c.fetchone()
    if k != None:
        li = []
        for x in k:
            li.append(x)
        return li
    else:
        return None


def ftokn(by, fr):
    c = conn.cursor()
    date = datetime.datetime.now()
    c.executemany(f"INSERT INTO ftkn (ffor, oby, dot) VALUES (?,?,?)",[(by, fr, date)])
    conn.commit()

def fbal(who):
    c = conn.cursor()
    z = 0
    k = c.execute(f"SELECT * FROM ftkn WHERE ffor = {who} and used = 0")
    for x in k:
        z += 1
    return z

def cbal(who,oby):
    c = conn.cursor()
    z = 0
    k = c.execute(f"SELECT * FROM ftkn WHERE ffor = {who} AND oby = {oby} AND used = 0")
    for x in k:
        z += 1
    return z

def use(who,oby):
    c = conn.cursor()
    c.execute(f"UPDATE ftkn SET used = 1 WHERE ffor = {who} AND oby = {oby} AND used = 0 LIMIT 1")
    conn.commit()

