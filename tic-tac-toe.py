import random 

def TicTacToe(p1,p2=None):
    slot = {1:1,2:2,3:3,4:4,5:5,6:6,7:7,8:8,9:9}
    nums = [0,1]
    slots = [1,2,3,4,5,6,7,8,9]
    turn = "To be decided"
    print(turn)
    chance = random.choice(nums)
    slots_filled = []
    board ="""
1 | 2 | 3
—————————
4 | 5 | 6
—————————
7 | 8 | 9\n"""
    print(board)

    def symbol(chance):
            if chance == 0:
                return "o"
            else:
                return "x"

    def tie():
        z = 0 
        for x in slots_filled:
            z += 1
        if z == 9:
            return True
        else:
            return False

    def victory():
        if slot[1] == slot[2] and slot[2] == slot[3]:
            return True
        elif slot[1] == slot[4] and slot[4] == slot[7]:
            return True
        elif slot[1] == slot[5] and slot[5] == slot[9]:
            return True
        elif slot[2] == slot[1] and slot[1] == slot[3]:
            return True
        elif slot[2] == slot[5] and slot[5] == slot[8]:
            return True
        elif slot[3] == slot[6] and slot[6] == slot[9]:
            return True
        elif slot[3] == slot[5] and slot[5] == slot[7]:
            return True
        elif slot[4] == slot[5] and slot[5] == slot[6]:
            return True
        elif slot[7] == slot[8] and slot[8] == slot[9]:
            return True
        elif slot[8] == slot[5] and slot[5] == slot[2]:
            return True
        elif tie():
            return "TIE"
        else:
            return False

    def slot_check(inp:int,chance):
        if inp in slots_filled:
            print('Slot Occupied select a different one')
            return False
        else:
            slots_filled.append(inp)
            slot[inp] = symbol(chance=chance)
            return True

    def bot_choice():
        inp = random.choice(slots)
        if inp in slots_filled:
            bot_choice()
        else:
            slots_filled.append(inp)
            slot[inp] = symbol(1)
            return True
    

    def agaisnt_bot(chance):
        count = 1
        while count == 1:
            if chance == 0:
                turn = f"{p1}'s turn"
                print(turn)
                try:
                    inp = int(input("Enter the slot left \n"))
                    is_int = slot_check(inp,0)
                except:
                    print('Enter A Valid Input \n')
                    agaisnt_bot(0)
                board = f"""
                            {slot[1]} | {slot[2]} | {slot[3]}
                            —————————
                            {slot[4]} | {slot[5]} | {slot[6]}
                            —————————
                            {slot[7]} | {slot[8]} | {slot[9]}\n"""
                print(board)
                if is_int:
                    chance = 1
                    result = victory()
                    if result == True:
                        count = 0
                        print(f'{p1} Won')
                    elif result == "TIE":
                        print("It's a tie")
                        count = 0
                else:
                    agaisnt_bot(0)
            else:
                turn = f"Bot Turn"
                print(turn)
                chance = 0
                inp = bot_choice()
                
                board = f"""
                            {slot[1]} | {slot[2]} | {slot[3]}
                            —————————
                            {slot[4]} | {slot[5]} | {slot[6]}
                            —————————
                            {slot[7]} | {slot[8]} | {slot[9]}\n"""
                print(board)
                result = victory()
                if result == True:
                    count = 0
                    print(f'Bot Won \n')
                elif result == "TIE":
                    print("It's a tie")
                    count = 0

    def against_human(chance):
        count = 1
        while count == 1:
            if chance == 0:
                turn = f"{p1}'s turn"
                print(turn)
                try:
                    inp = int(input("Enter the slot left \n \n"))
                    is_int = slot_check(inp,0)
                except:
                    print('Enter A Valid Input')
                    against_human(0)

                if is_int:
                    board = f"""
                            {slot[1]} | {slot[2]} | {slot[3]}
                            —————————
                            {slot[4]} | {slot[5]} | {slot[6]}
                            —————————
                            {slot[7]} | {slot[8]} | {slot[9]}\n"""
                    print(board)
                    chance = 1
                    result = victory()
                    if result == True:
                        count = 0
                        print(f'{p1} Won')
                    elif result == "TIE":
                        print("It's a tie")
                        count = 0
            else:
                turn = f"{p2}'s turn"
                print(turn)
                try:
                    inp = int(input("Enter the slot left \n"))
                    is_int = slot_check(inp,1)
                except:
                    print('Enter A Valid Input')
                    against_human(1)
                if is_int:
                    board = f"""
                            {slot[1]} | {slot[2]} | {slot[3]}
                            —————————
                            {slot[4]} | {slot[5]} | {slot[6]}
                            —————————
                            {slot[7]} | {slot[8]} | {slot[9]}\n"""
                    print(board)
                    chance = 0
                    result = victory()
                    if result == True:
                        count = 0
                        print(f'{p2} Won')
                    elif result == "TIE":
                        print("It's a tie")
                        count = 0
    if p2 is None:
        print(f'Enter the number of the left slots on your turn')
        agaisnt_bot(chance)
    else:
        print(f'Enter the number of the left slots on your turn')
        against_human(chance)

TicTacToe("Abhay","P2")