#!/bin/python3
import discord
from discord.utils import get
from discord.ext import commands
from passwd_dict import dict_call 

token = token=f"{dict_call('token')}"

bot = commands.Bot(command_prefix=commands.when_mentioned_or("stacy ","Stacy ","stacy","Stacy",'Stacy','sTacy','stAcy','staCy','stacY','STacy','STacy','STAcy','STaCy','STacY','STAcy','STAcy','STAcy','STACy','STAcY','STACy','STACy','STACy','STACy','STACY','STACY','STACY','STACY','STACY','STACY','StAcy','STAcy','StAcy','StACy','StAcY','StACy','STACy','StACy','StACy','StACY'), case_insensitive=True)

@bot.event
async def on_ready():
    print("Running")

@bot.event
async def on_message(message):
    if (message.content.startswith("http") or message.content.find("http") != -1) and message.guild.id == 829743733577744416:
        if message.channel.category_id != 829964476056076308 :
            if message.author.id != 452035187052642314:
                if not message.content.startswith("https://onlyfans"):
                    await message.delete()

@bot.event
async def on_raw_reaction_add(payload):
    channel = await bot.fetch_channel(payload.channel_id)
    message = await channel.fetch_message(payload.message_id)
    if payload.member.guild.id == 829743733577744416:
        if payload.message_id == 832309709276971079:
            if str(payload.emoji) == '♀️':
                Role = "Female"
                role = get(payload.member.guild.roles, name=Role)
                if str(payload.member.top_role) != "Male":
                    await payload.member.add_roles(role)
                await message.remove_reaction(emoji=payload.emoji,member=payload.member)
            elif str(payload.emoji) == '♂️':
                Role = "Male"
                role = get(payload.member.guild.roles, name=Role)
                if str(payload.member.top_role) != "Female":
                    await payload.member.add_roles(role)
                await payload.member.add_roles(role)
                await message.remove_reaction(emoji=payload.emoji,member=payload.member)
            else:
                await message.remove_reaction(emoji=payload.emoji,member=payload.member)

bot.run(token)
